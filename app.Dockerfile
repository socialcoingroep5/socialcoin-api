FROM tomee:8-jre-7.1.0-webprofile

RUN ["rm", "-rf", "/usr/local/tomee/webapps/ROOT"]
COPY ./target/socialcoin-1.0-SNAPSHOT.war /usr/local/tomee/webapps/ROOT.war

#tomee port
EXPOSE 8080
