package nl.socialcoin.dataaccess;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;
import nl.socialcoin.dto.Transfer;
import nl.socialcoin.general.MongoDB;
import org.bson.Document;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class TransferInformationDAO {

    @Inject
    private MongoDB mongoDB;

    public List<Transfer> transfersForDebt(int debtId) {
        List<Transfer> transfers = new ArrayList<>();
        try {

            BasicDBObject query = new BasicDBObject();
            query.put("debtId", debtId);
            Transfer transfer = null;
            MongoCursor<Document> dbObj = this.mongoDB.getConnection().getCollection("transfer").find(query).iterator();
            while(dbObj.hasNext()){
                transfer = this.mongoDB.convertToGenericObjectFromMongoDocument(Transfer.class, dbObj.next());
                transfers.add(transfer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transfers;
    }

    public List<Transfer> allTransfersInSystem() {
        List<Transfer> transfers = new ArrayList<>();
        try {
            MongoCursor<Document> dbObj = this.mongoDB.getConnection().getCollection("transfer").find().iterator();
            while(dbObj.hasNext()){
                Transfer transfer = this.mongoDB.convertToGenericObjectFromMongoDocument(Transfer.class, dbObj.next());
                transfers.add(transfer);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return transfers;
    }
}
