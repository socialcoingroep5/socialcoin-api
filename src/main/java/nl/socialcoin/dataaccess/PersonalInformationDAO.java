package nl.socialcoin.dataaccess;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import nl.socialcoin.dto.CityWorker;
import nl.socialcoin.dto.Creditor;
import nl.socialcoin.dto.DebtorInformation;
import nl.socialcoin.general.Database;
import nl.socialcoin.general.MongoDB;
import org.bson.Document;

import javax.inject.Inject;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;

public class PersonalInformationDAO {

    @Inject
    private Database database;

    @Inject
    private MongoDB mongoDB;

    @Context
    private HttpServletRequest request;

    public DebtorInformation retrieveDebtor(long debtorId) {
        DebtorInformation debtor = new DebtorInformation();
        try {

            BasicDBObject query = new BasicDBObject();
            query.put("debtorId", debtorId);

            MongoCursor<Document> dbObj = this.mongoDB.getConnection().getCollection("debtor").find(query).iterator();
            if(dbObj.hasNext()){
                debtor = this.mongoDB.convertToGenericObjectFromMongoDocument(DebtorInformation.class, dbObj.next());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return debtor;
    }

    public boolean registerDebtor(DebtorInformation debtorInformation) {
        System.out.println(debtorInformation);
        boolean success = false;
        try (Connection connection = database.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement callableStatement = connection.prepareCall("SELECT * FROM Insert_Debtor(?, ?)")) {
                callableStatement.setInt(1, debtorInformation.getWorkerId());
                callableStatement.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try(ResultSet rs = callableStatement.executeQuery()){
                    if(rs.next()){
                        debtorInformation.setDebtorId(rs.getInt("debtor_id"));
                    }else{
                        connection.rollback();
                        return false;
                    }
                }catch (Exception e){
                    connection.rollback();
                    e.printStackTrace();
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
                connection.rollback();
                return false;
            }

            try {
                // insert into blockchain/ nosql database
                MongoDatabase mongo = this.mongoDB.getConnection();
                mongo.getCollection("debtor").insertOne(this.mongoDB.convertToDocument(debtorInformation));
            } catch (Exception e) {
                e.printStackTrace();
                connection.rollback();
                return false;
            }
            success = true;
            connection.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
        // multi system transaction management.
        return success;
    }

    public boolean registerCreditor(Creditor creditor) {
        boolean success = false;
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("CALL Insert_Creditor(CAST(? as name), CAST(? as ip_address))")){
                cs.setString(1, creditor.getCreditorName());
                cs.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For") );
                cs.execute();
                success = true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }

    public boolean registerCityWorker(CityWorker cityWorker) {
        boolean success = false;
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("CALL Insert_City_Worker(CAST(? as name), CAST(? as name), CAST(? as ip_address))")){
                cs.setString(1, cityWorker.getCityName());
                cs.setString(2, cityWorker.getName());
                cs.setString(3, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }

    public Creditor retrieveCreditor(int creditorId) {
        Creditor creditor = null;
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Creditors(CAST(? as ip_address)) WHERE creditor_id = ?")){
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.setInt(2, creditorId);
                cs.execute();
                try(ResultSet rs = cs.executeQuery()){
                    if(rs.next()){
                        creditor = new Creditor();
                        creditor.setCreditorId(rs.getInt("creditor_id"));
                        creditor.setCreditorName(rs.getString("creditor_name"));
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return creditor;
    }

    public List<Creditor> retrieveCreditors() {
        List<Creditor> creditors = new ArrayList<>();
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Creditors(CAST(? as ip_address))")){
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                try(ResultSet rs = cs.executeQuery()){
                    while(rs.next()){
                        Creditor creditor = new Creditor();
                        creditor.setCreditorId(rs.getInt("creditor_id"));
                        creditor.setCreditorName(rs.getString("creditor_name"));
                        creditors.add(creditor);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return creditors;
    }
}
