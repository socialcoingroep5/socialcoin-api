package nl.socialcoin.dataaccess;

import com.mongodb.client.MongoDatabase;
import nl.socialcoin.dto.*;
import nl.socialcoin.general.Database;
import nl.socialcoin.general.MongoDB;
import nl.socialcoin.general.Status;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class TaskInformationDAO {

    @Inject
    private Database database;

    @Inject
    private MongoDB mongoDB;

    @Inject
    private TransferInformationDAO transferInformationDAO;

    @Inject
    private DebtInformationDAO debtInformationDAO;

    @Context
    private HttpServletRequest request;

    public boolean addTask(Task task) {
        boolean success = false;
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("CALL Insert_Task(CAST(? as id),CAST(? as id), ?, CAST(? AS status), CAST(? AS socialcoins), CAST(? AS date), CAST(? AS DATE), CAST(? AS ip_address))")) {
                if (task.getStatus().equals(Status.SUGGESTED)) { // suggested tasks wont have a worker id
                    cs.setLong(2, task.getDebtorId());
                    cs.setString(1, null);
                } else {
                    cs.setString(2, null); // if it is not a suggested task the debtorid will be null.
                    cs.setInt(1, task.getWorkerId());
                }
                cs.setString(3, task.getTaskName());
                cs.setString(4, task.getStatus().toString());
                cs.setInt(5, task.getSocialcoins());
                cs.setString(6, task.getBeginDate());
                cs.setString(7, task.getEndDate());
                cs.setString(8, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public boolean assignTask(int debtorId, int taskId) {
        boolean success = false;
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("CALL assignTask(CAST(? as id), CAST(? as id), CAST(? as ip_address))")) {
                cs.setInt(1, debtorId);
                cs.setInt(2, taskId);
                cs.setString(3, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public List<Task> tasksAvailable() {
        List<Task> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Task('OPEN', CAST(? AS ip_address))")) {
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        tasks.add(generateTask(rs));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    private Task generateTask(ResultSet rs) throws SQLException {
        Task task = new Task();
        task.setTaskId(rs.getInt("task_id"));
        task.setWorkerId(rs.getInt("worker_id"));
        task.setTaskName(rs.getString("task_name"));
        task.setSocialcoins(rs.getInt("socialcoins"));
        // Spaghetti? replacing all redundant spaces that magically appear.
        task.setStatus(Status.valueOf(rs.getString("status").replace(" ", "").toUpperCase()));
        task.setBeginDate(rs.getDate("begin_date").toString());
        task.setEndDate(rs.getDate("end_date").toString());
        return task;
    }


    public List<AssignedTask> tasksAssignedToDebtor(int debtorId) {
        List<AssignedTask> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_AssignedTasksToDebtor(CAST(? as id), CAST(? as ip_address))")) {
                cs.setInt(1, debtorId);
                cs.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        generateAssignedTask(tasks, rs);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }


    public List<AssignedTask> tasksAssigned() {
        List<AssignedTask> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_AssignedTasks(CAST(? AS ip_address))")) {
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        generateAssignedTask(tasks, rs);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    private void generateAssignedTask(List<AssignedTask> tasks, ResultSet rs) throws SQLException {
        AssignedTask assignedTask = new AssignedTask();
        assignedTask.setDebtorId(rs.getInt("debtor_id"));
        assignedTask.setTaskId(rs.getInt("task_id"));
        assignedTask.setFinishDate(rs.getDate("finish_date") == null ? "" : rs.getDate("finish_date").toString());
        tasks.add(assignedTask);
    }


    public boolean validateTask(int debtId, int taskId, boolean validated) {
        boolean success = false;
        try (Connection connection = database.getConnection()) {
            connection.setAutoCommit(false);
            try (CallableStatement cs = connection.prepareCall("CALL validate_task(?, CAST(? AS status), CAST(? AS ip_address))")) {
                cs.setInt(1, taskId);
                cs.setString(2, validated ? Status.APPROVED.toString() : Status.DENIED.toString());
                cs.setString(3, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
                // insert transfer for money
            } catch (Exception e) {
                e.printStackTrace();
                connection.rollback();
                return false;
            }
            try {
                MongoDatabase mongo = this.mongoDB.getConnection();
                Transfer transfer = new Transfer();
                Task task = this.allTasks().stream().filter(x -> x.getTaskId() == taskId).findFirst().orElse(null);
                if (task == null) {
                    connection.rollback();
                    return false;
                }
                transfer.setAmount(task.getSocialcoins());
                transfer.setDebtId(debtId);
                transfer.setTaskId(taskId);
                mongo.getCollection("transfer").insertOne(this.mongoDB.convertToDocument(transfer));
            } catch (Exception e) {
                e.printStackTrace();
                connection.rollback();
                return false;
            }
            try {
                if (debtIsPayedOff(debtId)) {
                    this.payOffDebt(debtId);
                }
            } catch (Exception e) {
                e.printStackTrace();
                connection.rollback();
                return false;
            }
            connection.commit();

        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public boolean debtIsPayedOff(int debtId) throws Exception {
        int debtorId = this.debtInformationDAO.getDebtorIdForDebt(debtId);
        List<MixedDebt> debts = this.debtInformationDAO.debtsForDebtor(debtorId);
        MixedDebt mixedDebt = debts.stream().filter(x -> x.getDebtInEuro().getDebtId() == debtId).findFirst().orElse(null);
        if (mixedDebt == null) {
            return false;
        }
        Debt debt = mixedDebt.getDebtInEuro();
        SocialCoinDebt debtInSocialCoin = mixedDebt.getDebtInSocialCoin();
        // get all transfers for a debt and sum the amount earned and compare that to the debt.
        List<Transfer> transfers = this.transferInformationDAO.transfersForDebt(debt.getDebtId());
        double totalObtained = transfers.stream().mapToDouble(i -> i.getAmount()).sum();
        double debtSize = debtInSocialCoin.getDebtInSocialCoin();
        return totalObtained >= debtSize;
    }

    public void payOffDebt(int debtId) throws Exception {
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("CALL pay_off_debt(CAST(? AS id), CAST(? as ip_address))")) {
                cs.setInt(1, debtId);
                cs.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
            } catch (Exception e) {
                throw e;
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public List<TaskWithActivities> tasksCompleted() {
        List<TaskWithActivities> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Task(CAST(? as status), CAST(? as ip_address))")) {
                cs.setString(1, Status.APPROVED.toString());
                cs.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        TaskWithActivities taskWithActivities = new TaskWithActivities();
                        taskWithActivities.setTask(generateTask(rs));
                        taskWithActivities.setActivities(new ArrayList<>());
                        try (CallableStatement cs2 = connection.prepareCall("SELECT * FROM Retrieve_TaskActivities(CAST(? as id), CAST(? AS ip_address))")) {
                            cs2.setInt(1, rs.getInt("task_id"));
                            cs2.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                            try (ResultSet rs2 = cs2.executeQuery()) {
                                while (rs2.next()) {
                                    Activity activity = generateActivity(rs2);
                                    taskWithActivities.getActivities().add(activity);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        tasks.add(taskWithActivities);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    private Activity generateActivity(ResultSet rs2) throws SQLException {
        Activity activity = new Activity();
        activity.setApproved(rs2.getBoolean("approved"));
        activity.setDate(rs2.getDate("date").toString());
        activity.setDebtorId(rs2.getInt("debtor_id"));
        activity.setTaskId(rs2.getInt("task_id"));
        activity.setHours(rs2.getInt("hours"));
        activity.setProof(rs2.getString("proof"));
        activity.setDescription(rs2.getString("description"));
        return activity;
    }


    //TODO make it log who suggested the task
    public boolean suggestTask(int debtorId, Task task) {
        task.setStatus(Status.SUGGESTED);
        task.setDebtorId(debtorId);
        return this.addTask(task);
    }

    public List<Task> allTasks() {
        List<Task> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Tasks(CAST(? AS ip_address))")) {
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        tasks.add(generateTask(rs));
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    // TODO there is no debtorId so you cant get the tasks for a debtor
    public List<Task> allTasksForDebtor(long debtorID) {
        List<Task> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_Tasks(CAST(? AS ip_address))")) {
                cs.setString(1, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    public List<Activity> activitiesAssociatedWithTask(int taskId) {
        List<Activity> tasks = new ArrayList<>();
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("SELECT * FROM Retrieve_TaskActivities(CAST(? as id), CAST(? AS ip_address))")) {
                cs.setInt(1, taskId);
                cs.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                try (ResultSet rs = cs.executeQuery()) {
                    while (rs.next()) {
                        tasks.add(generateActivity(rs));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return tasks;
    }

    public boolean addActivity(int taskId, int debtorId, Activity activity) {
        boolean success = false;
        try (Connection connection = database.getConnection()) {
            try (CallableStatement cs = connection.prepareCall("CALL insert_activity(?, ?, CURRENT_DATE, null , ?, ?, ?, ?, CAST(? AS ip_address))")) {
                cs.setInt(1, debtorId);
                cs.setInt(2, taskId);
                cs.setInt(3, activity.getHours());
                cs.setString(4, activity.getProof());
                cs.setBoolean(5, false);
                cs.setString(6, activity.getDescription());
                cs.setString(7, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return success;
    }

    public boolean approveActivity(int taskId, String date) {
        boolean success = false;
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("CALL validate_activity(?, CAST(? as date), CAST(? as ip_address))")){
                cs.setInt(1, taskId);
                cs.setString(2, date);
                cs.setString(3, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                cs.execute();
                success = true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }
}
