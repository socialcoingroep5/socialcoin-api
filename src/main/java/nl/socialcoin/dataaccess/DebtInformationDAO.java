package nl.socialcoin.dataaccess;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import nl.socialcoin.dto.Debt;
import nl.socialcoin.dto.DebtInSocialCoin;
import nl.socialcoin.dto.MixedDebt;
import nl.socialcoin.dto.SocialCoinDebt;
import nl.socialcoin.general.Database;
import nl.socialcoin.general.MongoDB;
import org.bson.Document;

import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.core.Context;
import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class DebtInformationDAO {

    @Inject
    private Database database;

    @Inject
    private MongoDB mongoDB;

    @Context
    private HttpServletRequest request;

    public boolean assignDebtInSocialCoin(DebtInSocialCoin debtInSocialCoin) {
        boolean success = false;
        try{
            MongoDatabase mongo = this.mongoDB.getConnection();
            mongo.getCollection("debt").insertOne(this.mongoDB.convertToDocument(debtInSocialCoin));
            success = true;
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }


    public boolean assignDebtInEuro(Debt debt) {
        boolean success = false;
        try(Connection connection = this.database.getConnection()){
            //TODO design decision for the use of current date instead of making the front end responsible
            try(CallableStatement callableStatement = connection.prepareCall("CALL Insert_Open_Debt(? , CAST(? as id), CURRENT_DATE,CAST(? AS DECIMAL(13,2)), CAST(? as ip_address))")){
                callableStatement.setString(1, debt.getCreditorName());
                callableStatement.setInt(2, debt.getDebtorId());
                callableStatement.setDouble(3, debt.getAmountInEuro());
                callableStatement.setString(4, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));
                callableStatement.execute();
                success = true;
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return success;
    }

    public List<MixedDebt> debtsForDebtor(int debtorId) {
        List<SocialCoinDebt> debtInSocialCoin = new ArrayList<>();
        List<Debt> debtsInEuro = new ArrayList<>();
        List<MixedDebt> mixedDebts = new ArrayList<>();
        try(Connection connection = this.database.getConnection()){
            try(CallableStatement callableStatement = connection.prepareCall("SELECT * FROM Retrieve_Debt_In_Euro(CAST(? as id), CAST(? as ip_address))")){
                callableStatement.setLong(1, debtorId);
                callableStatement.setString(2, request.getHeader("X-Forwarded-For") == null ? request.getRemoteAddr() : request.getHeader("X-Forwarded-For"));

                try(ResultSet rs = callableStatement.executeQuery()){
                    while(rs.next()){
                        Debt debtInEuro = new Debt();
                        debtInEuro.setAmountInEuro(NumberFormat.getNumberInstance(Locale.FRANCE).parse(rs.getString("amount_in_euro").replace("$", "")).doubleValue());
                        debtInEuro.setCreditorName(rs.getString("creditor_name"));
                        debtInEuro.setDebtDate(rs.getDate("debt_date").toString());
                        debtInEuro.setDebtorId(rs.getInt("debtor_id"));
                        debtInEuro.setDebtId(rs.getInt("debt_id"));
                        debtsInEuro.add(debtInEuro);
                    }
                }catch (Exception e){
                    e.printStackTrace();
                    return null; // error dont continue
                }
                // set the debt id so it can be retrieved from the other system.
            }catch (Exception e){
                e.printStackTrace();
                return null; // error dont continue
            }
        }catch (Exception e){
            e.printStackTrace();
            return null; // error dont continue
        }
        try{
            for(Debt debt : debtsInEuro) {
                BasicDBObject query = new BasicDBObject();
                query.put("debtId", debt.getDebtId());

                MongoCursor<Document> dbObj = this.mongoDB.getConnection().getCollection("debt").find(query).iterator();
                if (dbObj.hasNext()) {
                    DebtInSocialCoin debtInSocialCoin1 = this.mongoDB.convertToGenericObjectFromMongoDocument(DebtInSocialCoin.class, dbObj.next());

                    SocialCoinDebt socialCoinDebt = new SocialCoinDebt();
                    socialCoinDebt.setDebtId(debtInSocialCoin1.getDebtId());
                    socialCoinDebt.setDebtInEuro(debtInSocialCoin1.getDebtInEuro());
                    socialCoinDebt.setDebtInSocialCoin(debtInSocialCoin1.getDebtInSocialCoin());
                    debtInSocialCoin.add(socialCoinDebt);
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return null; // error dont continue
        }
        for(Debt debt : debtsInEuro){
            MixedDebt mixedDebt = new MixedDebt();
            mixedDebt.setDebtInEuro(debt);
            mixedDebt.setDebtInSocialCoin(debtInSocialCoin.stream().filter(x -> x.getDebtId() == debt.getDebtId()).findFirst().orElse(null));
            mixedDebts.add(mixedDebt);
            System.out.println(mixedDebt);
        }
        return mixedDebts;
    }

    public int getDebtorIdForDebt(int debtId) throws Exception {
        int debtorId = -1;
        try(Connection connection = database.getConnection()){
            try(CallableStatement cs = connection.prepareCall("SELECT debtor_id FROM debt_in_euro WHERE debt_id = ?")){
                cs.setInt(1, debtId);
                try(ResultSet rs = cs.executeQuery()){
                    if(rs.next()){
                        debtorId = rs.getInt("debtor_id");
                    }
                }catch (Exception e){
                    e.printStackTrace();
                }
            }catch (Exception e){
                e.printStackTrace();
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        if(debtorId == -1){
            throw new Exception();
        }
        return debtorId;
    }
}
