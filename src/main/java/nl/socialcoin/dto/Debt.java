package nl.socialcoin.dto;

import lombok.Data;

@Data
public class Debt {
    private int debtId;
    private String creditorName;
    private int debtorId;
    private String debtDate;
    private double amountInEuro;

}
