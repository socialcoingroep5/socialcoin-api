package nl.socialcoin.dto;

import lombok.Data;

import java.util.List;

@Data
public class TaskWithActivities {
    private Task task;
    private List<Activity> activities;

}
