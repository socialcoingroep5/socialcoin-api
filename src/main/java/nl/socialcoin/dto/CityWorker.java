package nl.socialcoin.dto;

import lombok.Data;

@Data
public class CityWorker {
    private String cityName;
    private int workerId;
    private String name;
}
