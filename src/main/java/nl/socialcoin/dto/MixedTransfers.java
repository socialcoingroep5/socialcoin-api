package nl.socialcoin.dto;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class MixedTransfers {
    private long debtId;
    private List<Transfer> transfers = new ArrayList<>();
}
