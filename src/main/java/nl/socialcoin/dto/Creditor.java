package nl.socialcoin.dto;

import lombok.Data;

@Data
public class Creditor {
    private int creditorId;
    private String creditorName;
}
