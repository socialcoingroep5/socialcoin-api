package nl.socialcoin.dto;

import lombok.Data;
import nl.socialcoin.general.Status;

@Data
public class Task {
    private long debtorId;
    private int taskId;
    private int workerId;
    private String taskName;
    private Status status;
    private int socialcoins;
    private String beginDate;
    private String endDate;

}
