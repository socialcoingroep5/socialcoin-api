package nl.socialcoin.dto;

import lombok.Data;

@Data
public class SocialCoinDebt {
    private long debtId;
    private double debtInEuro;
    private double debtInSocialCoin;
}
