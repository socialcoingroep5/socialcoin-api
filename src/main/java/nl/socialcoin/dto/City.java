package nl.socialcoin.dto;

import lombok.Data;

@Data
public class City {
    private String cityName;
}

