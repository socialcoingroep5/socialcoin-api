package nl.socialcoin.dto;

import lombok.Data;

@Data
public class MixedDebt {

    private Debt debtInEuro;
    private SocialCoinDebt debtInSocialCoin;
}
