package nl.socialcoin.dto;

import lombok.Data;

@Data
public class Transfer {
    private int taskId;
    private int debtId;
    private double amount;
}
