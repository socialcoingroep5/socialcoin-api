package nl.socialcoin.dto;

import lombok.Data;

@Data
public class DebtorInformation {
    private int debtorId;
    private String firstName;
    private String lastName;
    private String email;
    private String bsn;
    private String phoneNumber;
    private int cityCode;
    private int workerId;
}
