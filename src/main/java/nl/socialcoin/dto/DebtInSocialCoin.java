package nl.socialcoin.dto;

import lombok.Data;

@Data
public class DebtInSocialCoin {
    private long debtId;
    private double debtInEuro;
    private double debtInSocialCoin;
}
