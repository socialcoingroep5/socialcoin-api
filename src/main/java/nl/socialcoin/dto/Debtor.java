package nl.socialcoin.dto;

import lombok.Data;

@Data
public class Debtor {
    private int debtorId;
    private int workerId;

}
