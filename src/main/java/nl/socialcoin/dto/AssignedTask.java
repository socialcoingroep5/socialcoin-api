package nl.socialcoin.dto;

import lombok.Data;

import java.util.Date;
@Data
public class AssignedTask {
    private int debtorId;
    private int taskId;
    private String finishDate;


}
