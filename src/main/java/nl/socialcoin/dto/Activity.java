package nl.socialcoin.dto;

import lombok.Data;
@Data
public class Activity {
    private int debtorId;
    private int taskId;
    private String date;
    private int hours;
    private String proof;
    private boolean approved;
    private String description;
}
