package nl.socialcoin.general;

import javax.annotation.PostConstruct;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.http.HttpServlet;
import javax.ws.rs.core.Context;

@javax.servlet.annotation.WebListener
public class WebListener extends HttpServlet implements ServletContextListener {
    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {

    }


    @Context
    private ServletContext context;

    @PostConstruct
    public void init() {
        // init instance
    }
}
