package nl.socialcoin.general;

import com.mongodb.MongoClient;
import com.mongodb.MongoCredential;
import com.mongodb.client.MongoDatabase;
import lombok.Data;
import org.bson.Document;

import javax.inject.Singleton;
import java.lang.reflect.Field;

@Singleton
@Data
public class MongoDB {
    private MongoDatabase mongoDatabase;


    public MongoDatabase getConnection(){
        if(this.mongoDatabase == null){
            // Creating a Mongo client
            MongoClient mongo = new MongoClient( "localhost" , 27017 );
            // Creating Credentials
            MongoCredential credential;
            credential = MongoCredential.createCredential("root", "socialcoin",
                    "password".toCharArray());
            System.out.println("Connected to the mongodb database successfully");

            // Accessing the database
            this.mongoDatabase = mongo.getDatabase("socialcoin");
            return this.mongoDatabase;
        }else {
            return this.mongoDatabase;
        }
    }


    public void init(){
        MongoDatabase mongoDatabase = this.getConnection();
        mongoDatabase.createCollection("transfer");
        mongoDatabase.createCollection("debtor");
        mongoDatabase.createCollection("debt");

//        Document bsonDocument = new Document();
//        Document bsonDocument = null;
//        try {
//            bsonDocument = convertToDocument(debtor);
//        } catch (IllegalAccessException e) {
//            e.printStackTrace();
//        }
//        MongoCollection collection = this.mongoDatabase.getCollection("transfer");
//        collection.insertOne(bsonDocument);


//        MongoCursor<Document> cursor = this.mongoDatabase.getCollection("transfer").find().iterator();
//        while(cursor.hasNext()){
//            Document document = cursor.next();
//            try {
//                Debtor o = (Debtor) this.convertToGenericObjectFromMongoDocument(Debtor.class, document);
//            } catch (Exception e) {
//
//            }
//        }
    }



    public Document convertToDocument(Object o) throws IllegalAccessException {
        Document document = new Document();
        Field[] fields = o.getClass().getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            document.put(field.getName(), field.get(o)); // converts a generic object to the document form mongodb expects.
        }
        return document;
    }

    public <T> T convertToGenericObjectFromMongoDocument( Class<T> type, Document d) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        T o = type.newInstance();
        Field[] fields = type.getDeclaredFields();
        for(Field field : fields){
            field.setAccessible(true);
            field.set(o, d.get(field.getName()));
        }
        return o;
    }
}
