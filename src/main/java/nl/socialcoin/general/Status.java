package nl.socialcoin.general;

public enum Status {
    OPEN, SUGGESTED, APPROVED , IN_PROGRESS, COMPLETED, DENIED, ASSIGNED
}
