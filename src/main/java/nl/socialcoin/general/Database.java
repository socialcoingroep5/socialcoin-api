package nl.socialcoin.general;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;

import javax.inject.Singleton;
import java.io.IOException;
import java.sql.Connection;
import java.util.Properties;

@Singleton
public class
Database {
    private HikariDataSource dataSource;

    public Database() {
        init();
    }

    private boolean init() {
        try {
            openConnection();
            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private void openConnection() {
        Properties properties = new Properties();
        try {
            properties.load(getClass().getClassLoader().getResourceAsStream("settings.properties"));
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Connecting to database as: " + properties.getProperty("user"));
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl("jdbc:postgresql://" + properties.getProperty("host") + ":" + properties.getProperty("port") + "/" + properties.getProperty("database") + "");
        config.setDriverClassName("org.postgresql.Driver");
        config.setUsername(properties.getProperty("user"));
        config.setPassword(properties.getProperty("pass"));
        config.setMinimumIdle(Integer.parseInt(properties.getProperty("minimumIdle")));
        config.setMaximumPoolSize(Integer.parseInt(properties.getProperty("maximumPoolSize")));
        config.setIdleTimeout(Integer.parseInt(properties.getProperty("idleTimeOut")));
        config.setConnectionTimeout(Integer.parseInt(properties.getProperty("connectionTimeout")));
        config.setConnectionTestQuery(properties.getProperty("connectionTestQuery"));
        this.dataSource = new HikariDataSource(config);
        System.out.println("Connection established with database!: " + properties.getProperty("host") + ":" + properties.getProperty("port"));
    }


    public void destroy() {
        try {
            if (this.dataSource != null) {
                this.dataSource.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public Connection getConnection() {
        try {
            return this.dataSource.getConnection();
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * Get the {@link HikariDataSource}.
     */
    public HikariDataSource getDataSource() {
        return this.dataSource;
    }
}
