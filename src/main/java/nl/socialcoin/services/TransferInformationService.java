package nl.socialcoin.services;

import nl.socialcoin.dataaccess.TransferInformationDAO;
import nl.socialcoin.dto.MixedDebt;
import nl.socialcoin.dto.MixedTransfers;
import nl.socialcoin.dto.Transfer;

import javax.inject.Inject;
import java.util.ArrayList;
import java.util.List;

public class TransferInformationService {

    @Inject
    private DebtInformationService debtInformationService;

    @Inject
    private TransferInformationDAO transferInformationDAO;

    public List<MixedTransfers> transfersForDebtor(int debtorId) {
        List<MixedTransfers> mixedTransfers = new ArrayList<>();
        List<MixedDebt> debts = debtInformationService.debtsForDebtor(debtorId);
        for(MixedDebt debt : debts){
            List<Transfer> transfers = this.transferInformationDAO.transfersForDebt(debt.getDebtInEuro().getDebtId());
            MixedTransfers mixedTransfers1 = new MixedTransfers();
            mixedTransfers1.setDebtId(debt.getDebtInEuro().getDebtId());
            mixedTransfers1.setTransfers(transfers);
            mixedTransfers.add(mixedTransfers1);
        }
        return mixedTransfers;
    }

    public List<Transfer> transfersForDebt(int debtId) {
        return this.transferInformationDAO.transfersForDebt(debtId);
    }

    public List<Transfer> allTransfersInSystem() {
        return this.transferInformationDAO.allTransfersInSystem();
    }
}
