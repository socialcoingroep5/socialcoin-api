package nl.socialcoin.services;

import nl.socialcoin.dataaccess.PersonalInformationDAO;
import nl.socialcoin.dto.CityWorker;
import nl.socialcoin.dto.Creditor;
import nl.socialcoin.dto.DebtorInformation;

import javax.inject.Inject;
import java.util.List;

public class PersonalInformationService {

    @Inject
    private PersonalInformationDAO personalInformationDAO;


    public DebtorInformation retreiveDebtor(long debtorId){
      return this.personalInformationDAO.retrieveDebtor(debtorId);
    }

    public boolean registerDebtor(DebtorInformation debtorInformation) {
        if(!isValidBsn(debtorInformation.getBsn())){
            return false;
        }
        return this.personalInformationDAO.registerDebtor(debtorInformation);
    }


    private boolean isValidBsn(String input){
        int length = input.length();
        int total = 0;
        for(int i = 0; i < length; i++){
            int multiplier = length - i;
            if(multiplier == 1){
                multiplier = -1;
            }
            total += multiplier * Integer.parseInt(input.substring(i, i + 1));
        }
        return total % 11 == 0;
    }

    public boolean registerCreditor(Creditor creditor) {
        return this.personalInformationDAO.registerCreditor(creditor);
    }

    public boolean registerCityWorker(CityWorker cityWorker) {
        return this.personalInformationDAO.registerCityWorker(cityWorker);
    }

    public Creditor retrieveCreditor(int creditorId) {
        return this.personalInformationDAO.retrieveCreditor(creditorId);
    }

    public List<Creditor> retrieveCreditors() {
        return this.personalInformationDAO.retrieveCreditors();
    }
}
