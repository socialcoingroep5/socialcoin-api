package nl.socialcoin.services;

import nl.socialcoin.dataaccess.DebtInformationDAO;
import nl.socialcoin.dto.Debt;
import nl.socialcoin.dto.DebtInSocialCoin;
import nl.socialcoin.dto.MixedDebt;

import javax.inject.Inject;
import java.util.List;

public class DebtInformationService {

    @Inject
    private DebtInformationDAO debtInformationDAO;


    public List<MixedDebt> debtsForDebtor(int debtorId) {
        return this.debtInformationDAO.debtsForDebtor(debtorId);
    }

    public boolean assignDebtInSocialCoin(DebtInSocialCoin debtInSocialCoin) {
        return this.debtInformationDAO.assignDebtInSocialCoin(debtInSocialCoin);
    }

    public boolean assignDebtInEuro(Debt debt) {
        return this.debtInformationDAO.assignDebtInEuro(debt);
    }
}
