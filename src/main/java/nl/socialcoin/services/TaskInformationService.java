package nl.socialcoin.services;

import nl.socialcoin.dataaccess.TaskInformationDAO;
import nl.socialcoin.dto.Activity;
import nl.socialcoin.dto.AssignedTask;
import nl.socialcoin.dto.Task;
import nl.socialcoin.dto.TaskWithActivities;

import javax.inject.Inject;
import java.util.List;

public class TaskInformationService {

    @Inject
    private TaskInformationDAO taskInformationDAO;


    public boolean addTask(Task task) {
        return this.taskInformationDAO.addTask(task);
    }

    public boolean assignTask(int debtorId, int taskId) {
        return this.taskInformationDAO.assignTask(debtorId, taskId);
    }

    public List<Task> tasksAvailable() {
        return this.taskInformationDAO.tasksAvailable();
    }

    public List<AssignedTask> tasksAssignedToDebtor(int debtorId){
        return this.taskInformationDAO.tasksAssignedToDebtor(debtorId);
    }

    public List<AssignedTask> tasksAssigned(){
        return this.taskInformationDAO.tasksAssigned();
    }

    public boolean validateTask(int debtId, int taskId, boolean validated){
        return this.taskInformationDAO.validateTask(debtId, taskId, validated);
    }

    public List<TaskWithActivities> tasksCompleted(){
        return this.taskInformationDAO.tasksCompleted();
    }


    public boolean suggestTask(int debtorId, Task task) {
        return this.taskInformationDAO.suggestTask(debtorId, task);
    }

    public List<Task> allTasks() {
        return this.taskInformationDAO.allTasks();
    }


    public List<Activity> activitiesAssociatedWithTask(int taskId) {
        return this.taskInformationDAO.activitiesAssociatedWithTask(taskId);
    }

    public boolean addActivity(int taskId, int debtorId, Activity activity) {
        return this.taskInformationDAO.addActivity(taskId, debtorId, activity);
    }

    public boolean approveActivity(int taskId, String date) {
        return this.taskInformationDAO.approveActivity(taskId, date);
    }
}
