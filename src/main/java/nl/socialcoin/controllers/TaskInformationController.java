/*
 * Author: Steven Krol
 */
package nl.socialcoin.controllers;

import nl.socialcoin.dto.Activity;
import nl.socialcoin.dto.Task;
import nl.socialcoin.services.TaskInformationService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("task")
public class TaskInformationController {

    @Inject
    private TaskInformationService taskInformationService;



    /**
     * Add task to system
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addTask(Task task){
        return Response.status(200).entity(this.taskInformationService.addTask(task)).build();
    }

    /**
     * assign task to debtor
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("assign/{debtorId}/{taskId}")
    public Response assignTask(@PathParam("debtorId") int debtorId, @PathParam("taskId") int taskId){
        return Response.status(200).entity(this.taskInformationService.assignTask(debtorId, taskId)).build();
    }

    /**
     * Suggest task
     * @return
     */
    @POST
    @Path("suggest/{debtorId}/")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response suggestTask(@PathParam("debtorId") int debtorId, Task task){
        return Response.status(200).entity(this.taskInformationService.suggestTask(debtorId, task)).build();
    }

    /**
     * All tasks that are available
     * @return
     */
    @GET
    @Path("available")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tasksAvailable(){
        return Response.status(200).entity(this.taskInformationService.tasksAvailable()).build();
    }

    /**
     * All tasks that are completed
     * @return
     */
    @GET
    @Path("completed")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tasksCompleted(){
        return Response.status(200).entity(this.taskInformationService.tasksCompleted()).build();
    }

    /**
     * All tasks that are assinged t a certain debtor
     * @return
     */
    @GET
    @Path("assigned/{debtorId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tasksAssignedToDebtor(@PathParam("debtorId") int debtorId){
        return Response.status(200).entity(this.taskInformationService.tasksAssignedToDebtor(debtorId)).build();
    }

    /**
     * All tasks that are assinged at this moment.
     * @return
     */
    @GET
    @Path("assigned")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response tasksAssigned(){
        return Response.status(200).entity(this.taskInformationService.tasksAssigned()).build();
    }

    //TODO ADMIN AUTHENTICATION
    @POST
    @Path("validate/{taskId}/{debtId}/{validated}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response validateTask(@PathParam("debtId") int debtId, @PathParam("taskId") int taskId, @PathParam("validated") boolean validated){
        return Response.status(200).entity(this.taskInformationService.validateTask(debtId, taskId, validated)).build();
    }

    /**
     * All tasks that are in the system with a attribute completed set to true or false.
     * @return
     */
    @GET
    @Path("tasks")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response allTasks(){
        return Response.status(200).entity(this.taskInformationService.allTasks()).build();
    }



    /**
     * View task progress. this shows all activities that have been executed for a user
     * @return
     */
    @GET
    @Path("progress/{taskId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response viewTaskProgress(@PathParam("taskId") int taskId){
        return Response.status(200).entity(this.taskInformationService.activitiesAssociatedWithTask(taskId)).build();
    }


    /**
     * Add  activity to task as a debtor: when a debtor finished a part of the task. They can add that activity so that can later be viewed as progress.
     * @return
     */
    @POST
    @Path("activity/{debtorId}/{taskId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response addActivity(@PathParam("taskId") int taskId,@PathParam("debtorId") int debtorId,  Activity activity){
        boolean success = this.taskInformationService.addActivity(taskId, debtorId, activity);
        return Response.status(success ? 200 : 500).entity(success).build();
    }


    /**
     * Approve activity
     * @return
     */
    @POST
    @Path("activity/approve/{taskId}/{date}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    public Response approveActivity(@PathParam("taskId") int taskId, @PathParam("date") String date){
        return Response.status(200).entity(this.taskInformationService.approveActivity(taskId, date)).build();
    }
}
