package nl.socialcoin.controllers;

import nl.socialcoin.dto.CityWorker;
import nl.socialcoin.dto.Creditor;
import nl.socialcoin.dto.DebtorInformation;
import nl.socialcoin.services.PersonalInformationService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("personal")
public class PersonalInformationController {

    @Inject
    private PersonalInformationService personalInformationService;

    /**
     * Get all the personal personalInformation from a debtor
     *
     * @param debtorId
     * @return
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("retrieve/debtor/{debtorID}")
    public Response personalInformation(@PathParam("debtorID") long debtorId) {
        return Response.status(200).entity(this.personalInformationService.retreiveDebtor(debtorId)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("register/debtor")
    public Response registerDebtor(DebtorInformation debtorInformation) {
        return Response.status(200).entity(this.personalInformationService.registerDebtor(debtorInformation)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("register/creditor")
    public Response registerCreditor(Creditor creditor) {
        return Response.status(200).entity(this.personalInformationService.registerCreditor(creditor)).build();
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("register/cityworker")
    public Response registerCityWorker(CityWorker cityWorker) {
        return Response.status(200).entity(this.personalInformationService.registerCityWorker(cityWorker)).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("retrieve/creditor/{creditorId}")
    public Response retrieveCreditor(@PathParam("creditorId") int creditorId) {
        return Response.status(200).entity(this.personalInformationService.retrieveCreditor(creditorId)).build();
    }

    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("retrieve/creditor/")
    public Response retrieveCreditors() {
        return Response.status(200).entity(this.personalInformationService.retrieveCreditors()).build();
    }


}