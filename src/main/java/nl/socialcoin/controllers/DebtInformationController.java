package nl.socialcoin.controllers;

import nl.socialcoin.dto.Debt;
import nl.socialcoin.dto.DebtInSocialCoin;
import nl.socialcoin.services.DebtInformationService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("debt")
public class DebtInformationController {

    @Inject
    private DebtInformationService debtInformationService;


    /**
     * This requires the debtorId to be set in order to create a new debt for a debtor.
     * @param debt
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("assignEuro")
    public Response assignDebtInEuro(Debt debt){
        return Response.status(200).entity(debtInformationService.assignDebtInEuro(debt)).build();
    }


    /**
     * This requires the debtId to be set in order to link the debt in socialcoin to the debt in euro's.
     * @param debtInSocialCoin
     * @return
     */
    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.TEXT_PLAIN)
    @Path("assignSocialCoin")
    public Response assignDebtInSocialCoin(DebtInSocialCoin debtInSocialCoin){
        return Response.status(200).entity(debtInformationService.assignDebtInSocialCoin(debtInSocialCoin)).build();
    }

    /**
     * all debt for debtor in euro's and socialcoin
     * @return
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("{debtorId}")
    public Response getDebtsForDebtor(@PathParam("debtorId") int debtorId){
        return Response.status(200).entity(this.debtInformationService.debtsForDebtor(debtorId)).build();
    }



}
