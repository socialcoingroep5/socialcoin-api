package nl.socialcoin.controllers;

import nl.socialcoin.services.TransferInformationService;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("transfer")
public class TransferInformationController {

    @Inject
    private TransferInformationService transferInformationService;


    /**
     * returns all transfers for a debtor via it's debtorId.
     * @return
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("debtor/{debtorId}")
    public Response transfersForDebtor(@PathParam("debtorId") int debtorId){
        return Response.status(200).entity(this.transferInformationService.transfersForDebtor(debtorId)).build();
    }

    /**
     * returns all transfers for a debt
     * @return
     */
    @GET
    @Path("debt/{debtId}")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public Response transfersForDebt(@PathParam("debtId") int debtId){
        return Response.status(200).entity(this.transferInformationService.transfersForDebt(debtId)).build();
    }

    /**
     * returns all transfers in the system
     * @return
     */
    @GET
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("")
    public Response allTransfers(){
        return Response.status(200).entity(this.transferInformationService.allTransfersInSystem()).build();
    }
}
