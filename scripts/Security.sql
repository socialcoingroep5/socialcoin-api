
-- creating roles

-- super user for use of administrating the database
CREATE ROLE Administrator WITH SUPERUSER LOGIN PASSWORD 'admin';

-- creating the general city worker role
CREATE ROLE CityWorker;

-- creating a "user" instance of city worker.
CREATE ROLE Cassandra WITH LOGIN PASSWORD 'pass1 'IN ROLE CityWorker;


-- creating a general role for debtors
CREATE ROLE Debtor;

-- creating a "user" within the debtor role
CREATE ROLE Mats WITH LOGIN PASSWORD 'debtpass';

--creating a general role for creditors
CREATE ROLE Creditor;

-- creating a "user" for the creditor role
CREATE ROLE Duo WITH LOGIN PASSWORD 'duo';

-- granting rights for cityworker
GRANT EXECUTE ON  ALL PROCEDURES IN SCHEMA public TO CityWorker;
GRANT EXECUTE ON ALL FUNCTIONS IN SCHEMA  public TO CityWorker;

-- granting execution rights to creditor
GRANT EXECUTE ON FUNCTION  retrieve_debt_in_euro(id) TO creditor;
GRANT EXECUTE ON FUNCTION retrieve_debtor(id), retrieve_debtors() TO Creditor;


-- granting rights to Debtor
GRANT SELECT ON TABLE activity, assignedtask,debt_in_euro TO Debtor;



