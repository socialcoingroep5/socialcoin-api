DROP TRIGGER IF EXISTS TR_assignedtask_BI_update_history ON PUBLIC.assignedtask;
DROP TRIGGER IF EXISTS TR_task_BI_update_history ON PUBLIC.task;
DROP TRIGGER IF EXISTS TR_activity_BI_update_history ON PUBLIC.activity;
DROP TRIGGER IF EXISTS TR_debt_in_euro_BI_update_history ON PUBLIC.debt_in_euro;

-- Update valid_until column before insert in table assignedtask
CREATE OR replace FUNCTION TR_assignedtask_BI_update_history()
RETURNS TRIGGER AS $ASSIGNEDTASK_STAMP$
BEGIN
	IF EXISTS (
			SELECT 1
			FROM assignedtask
			WHERE debtor_id = new.debtor_id
				AND task_id = new.task_id
				AND valid_until IS NULL
			) then
		UPDATE assignedtask
		SET valid_until = current_timestamp
		WHERE debtor_id = new.debtor_id
			AND task_id = new.task_id
			AND valid_until IS NULL;
END IF;
RETURN NEW;
END $ASSIGNEDTASK_STAMP$
LANGUAGE plpgsql;


CREATE TRIGGER TR_assignedtask_BI_update_history BEFORE
    INSERT ON assignedtask
    FOR EACH ROW
EXECUTE PROCEDURE TR_assignedtask_BI_update_history();

-- Update valid_until column before insert in table task
CREATE OR replace FUNCTION TR_task_BI_update_history ()
RETURNS TRIGGER AS $TASK_STAMP$
BEGIN
	IF EXISTS (
			SELECT 1
			FROM task
			WHERE task_id = new.task_id
				AND valid_until IS NULL
			) then
		UPDATE task
		SET valid_until = current_timestamp
		WHERE task_id = new.task_id
			AND valid_until IS NULL;
END IF;
RETURN NEW;
END $TASK_STAMP$
LANGUAGE plpgsql;

CREATE TRIGGER TR_task_BI_update_history BEFORE
    INSERT ON task
    FOR EACH ROW
EXECUTE PROCEDURE TR_task_BI_update_history();

-- Update valid_until column before insert in table activity
CREATE OR replace FUNCTION TR_activity_BI_update_history ()
RETURNS TRIGGER AS $ACTIVITY_STAMP$
BEGIN
	IF EXISTS (
			SELECT 1
			FROM activity
			WHERE debtor_id = new.debtor_id
				AND task_id = new.task_id
				AND DATE = new.DATE
				AND valid_until IS NULL
			) then
		UPDATE activity
		SET valid_until = current_timestamp
		WHERE task_id = new.task_id
			AND debtor_id = new.debtor_id
			AND DATE = new.DATE
			AND valid_until IS NULL;
END IF;
RETURN NEW;
END $ACTIVITY_STAMP$
LANGUAGE plpgsql;

CREATE TRIGGER TR_activity_BI_update_history BEFORE
    INSERT ON activity
    FOR EACH ROW
EXECUTE PROCEDURE TR_activity_BI_update_history();

-- Update valid_until column before insert in table debt_in_euro
CREATE OR replace FUNCTION TR_debt_in_euro_BI_update_history ()
RETURNS TRIGGER AS $DEBT_IN_EURO_STAMP$
BEGIN
	IF EXISTS (
			SELECT 1
			FROM debt_in_euro
			WHERE debtor_id = new.debtor_id
				AND debt_date = new.debt_date
				AND creditor_name = new.creditor_name
				AND debt_id = new.debt_id
				AND valid_until IS NULL
			) then
		UPDATE debt_in_euro
		SET valid_until = current_timestamp
		WHERE debtor_id = new.debtor_id
			AND valid_until IS NULL;
END IF ;
RETURN NEW;
END $DEBT_IN_EURO_STAMP$
LANGUAGE plpgsql;

CREATE TRIGGER TR_debt_in_euro_BI_update_history BEFORE
    INSERT ON debt_in_euro
    FOR EACH ROW
EXECUTE PROCEDURE TR_debt_in_euro_BI_update_history();


