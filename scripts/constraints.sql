ALTER TABLE task DROP CONSTRAINT IF EXISTS CK_task_begindate_not_greater_than_enddate;


/*
  BR2: The start date of a task must have occurred before the stop date.
 */
ALTER TABLE task ADD CONSTRAINT CK_task_begindate_not_greater_than_enddate
CHECK (begin_date < end_date);
