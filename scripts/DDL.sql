/*==============================================================*/
/* DBMS name:      PostgreSQL 9.x                               */
/* Created on:     27-May-19 09:40:02                           */
/*==============================================================*/

drop index if exists CITY_WORKER_ACTIVITY_FK;

drop index if exists ASSIGNEDTASK_ACTIVITY_FK;

drop index if exists ACTIVITY_PK;

drop table if exists ACTIVITY;

drop index if exists ASSIGNEDTASK_TASK_FK;

drop index if exists DEPTOR_TASK_FK;

drop index if exists ASSIGNEDTASK_PK;

drop table if exists ASSIGNEDTASK;

drop index if exists CITY_PK;

drop table if exists CITY;

drop index if exists CITY_CITYWORKER_FK;

drop index if exists CITY_WORKER_PK;

drop table if exists CITY_WORKER;

drop index if exists CREDITOR_PK;

drop table if exists CREDITOR;

drop index if exists CITYWORKER_DEBTOR_FK;

drop index if exists DEBTOR_PK;

drop table if exists DEBTOR;

drop index if exists OWED_BY_FK;

drop index if exists CREDITOR_DEPT_FK;

drop index if exists DEBT_IN_EURO_PK;

drop table if exists DEBT_IN_EURO;

drop index if exists CITYWORKER_TASK_FK;

drop index if exists TASK_PK;

drop table if exists TASK;

drop domain if exists AMOUNT;

drop domain if exists APPROVED;

drop domain if exists BSN;

drop domain if exists CITYNAME;

drop domain if exists CREDITOR_NAME;

drop domain if exists DESCRIPTION;

drop domain if exists EMAIL;

drop domain if exists EURO;

drop domain if exists HOURS;

drop domain if exists ID;

drop domain if exists IDENTIFIER;

drop domain if exists KEY;

drop domain if exists PAYEDOFF;

drop domain if exists PHONE;

drop domain if exists PROOF;

drop domain if exists SOCIALCOIN;

drop domain if exists SOCIALCOINS;

drop domain if exists STATUS;

drop domain if exists TASK_NAME;

drop domain if exists "TIMESTAMP";

drop domain if exists WALLETNUMBER;

DROP DOMAIN IF EXISTS SP_NAME;

DROP DOMAIN IF EXISTS SP_PARAM;

DROP DOMAIN IF EXISTS QUERY_TYPE;

DROP DOMAIN IF EXISTS IP_ADDRESS;

DROP DOMAIN IF EXISTS "CURRENT_USER";

DROP TABLE IF EXISTS AUDIT;

/*==============================================================*/
/* Domain: AMOUNT                                               */
/*==============================================================*/
create domain AMOUNT as DECIMAL(13,2);

/*==============================================================*/
/* Domain: APPROVED                                             */
/*==============================================================*/
create domain APPROVED as BOOL;

/*==============================================================*/
/* Domain: BSN                                                  */
/*==============================================================*/
create domain BSN as INT4;

/*==============================================================*/
/* Domain: CITYNAME                                             */
/*==============================================================*/
create domain CITYNAME as TEXT;

/*==============================================================*/
/* Domain: CREDITOR_NAME                                        */
/*==============================================================*/
create domain CREDITOR_NAME as TEXT;

/*==============================================================*/
/* Domain: DESCRIPTION                                          */
/*==============================================================*/
create domain DESCRIPTION as TEXT;

/*==============================================================*/
/* Domain: EMAIL                                                */
/*==============================================================*/
create domain EMAIL as TEXT;

/*==============================================================*/
/* Domain: EURO                                                 */
/*==============================================================*/
create domain EURO as DECIMAL(8);

/*==============================================================*/
/* Domain: HOURS                                                */
/*==============================================================*/
create domain HOURS as INT4;

/*==============================================================*/
/* Domain: ID                                                   */
/*==============================================================*/
create domain ID as INT4;

/*==============================================================*/
/* Domain: IDENTIFIER                                           */
/*==============================================================*/
create domain IDENTIFIER as INT4;

/*==============================================================*/
/* Domain: KEY                                                  */
/*==============================================================*/
create domain KEY as TEXT;

/*==============================================================*/
/* Domain: PAYEDOFF                                             */
/*==============================================================*/
create domain PAYEDOFF as BOOL;

/*==============================================================*/
/* Domain: PHONE                                                */
/*==============================================================*/
create domain PHONE as INT4;

/*==============================================================*/
/* Domain: PROOF                                                */
/*==============================================================*/
create domain PROOF as TEXT;

/*==============================================================*/
/* Domain: SOCIALCOIN                                           */
/*==============================================================*/
create domain SOCIALCOIN as INT4;

/*==============================================================*/
/* Domain: SOCIALCOINS                                          */
/*==============================================================*/
create domain SOCIALCOINS as INT4;

/*==============================================================*/
/* Domain: STATUS                                               */
/*==============================================================*/
create domain STATUS as CHAR(256);

/*==============================================================*/
/* Domain: TASK_NAME                                            */
/*==============================================================*/
create domain TASK_NAME as TEXT;

/*==============================================================*/
/* Domain: "TIMESTAMP"                                          */
/*==============================================================*/
create domain "TIMESTAMP" as TIMESTAMP;

/*==============================================================*/
/* Domain: WALLETNUMBER                                         */
/*==============================================================*/
create domain WALLETNUMBER as INT4;

/*==============================================================*/
/* Domain: SP_NAME                                              */
/*==============================================================*/
CREATE DOMAIN SP_NAME as VARCHAR(64);

/*==============================================================*/
/* Domain: SP_PARAM                                             */
/*==============================================================*/
CREATE DOMAIN SP_PARAM as VARCHAR(64);

/*==============================================================*/
/* Domain: QUERY_TYPE                                           */
/*==============================================================*/
CREATE DOMAIN QUERY_TYPE as CHAR(6);

/*==============================================================*/
/* Domain: IP_ADDRESS                                           */
/*==============================================================*/
CREATE DOMAIN IP_ADDRESS as VARCHAR(45);

/*==============================================================*/
/* Domain: CURRENT_USER                                         */
/*==============================================================*/
CREATE DOMAIN "CURRENT_USER" as VARCHAR(64);

/*==============================================================*/
/* Table: ACTIVITY                                              */
/*==============================================================*/
create table ACTIVITY (
   DEBTOR_ID            ID                   not null,
   TASK_ID              ID                   not null,
   "TIMESTAMP"          "TIMESTAMP"          not null default current_timestamp,
   DATE                 DATE                 not null,
   WORKER_ID            ID                   null,
   HOURS                HOURS                not null,
   PROOF                PROOF                null,
   APPROVED             APPROVED             null,
   DESCRIPTION          DESCRIPTION          not null,
   VALID_UNTIL          "TIMESTAMP"          null,
   constraint PK_ACTIVITY primary key (DEBTOR_ID, TASK_ID, "TIMESTAMP", DATE)
);

/*==============================================================*/
/* Index: ACTIVITY_PK                                           */
/*==============================================================*/
create unique index ACTIVITY_PK on ACTIVITY (
DEBTOR_ID,
TASK_ID,
"TIMESTAMP",
DATE
);

/*==============================================================*/
/* Index: ASSIGNEDTASK_ACTIVITY_FK                              */
/*==============================================================*/
create  index ASSIGNEDTASK_ACTIVITY_FK on ACTIVITY (
DEBTOR_ID,
TASK_ID,
"TIMESTAMP"
);

/*==============================================================*/
/* Index: CITY_WORKER_ACTIVITY_FK                               */
/*==============================================================*/
create  index CITY_WORKER_ACTIVITY_FK on ACTIVITY (
WORKER_ID
);

/*==============================================================*/
/* Table: AUDIT                                                 */
/*==============================================================*/
create table AUDIT (
   AUDIT_ID             SERIAL               PRIMARY KEY,
   "TIMESTAMP"          "TIMESTAMP"          NOT NULL DEFAULT CURRENT_TIMESTAMP,
   SP_NAME              SP_NAME              NOT NULL,
   SP_PARAM             SP_PARAM             NULL,
   QUERY_TYPE           QUERY_TYPE           NOT NULL,
   IP_ADDRESS           IP_ADDRESS           NOT NULL,
   "CURRENT_USER"       "CURRENT_USER"       NOT NULL
);

/*==============================================================*/
/* Table: ASSIGNEDTASK                                          */
/*==============================================================*/
create table ASSIGNEDTASK (
   DEBTOR_ID            ID                   not null,
   TASK_ID              ID                   not null,
   "TIMESTAMP"          "TIMESTAMP"          not null default current_timestamp,
   FINISH_DATE          DATE                 null,
   VALID_UNTIL          "TIMESTAMP"          null,
   constraint PK_ASSIGNEDTASK primary key (DEBTOR_ID, TASK_ID, "TIMESTAMP")
);

/*==============================================================*/
/* Index: ASSIGNEDTASK_PK                                       */
/*==============================================================*/
create unique index ASSIGNEDTASK_PK on ASSIGNEDTASK (
DEBTOR_ID,
TASK_ID,
"TIMESTAMP"
);

/*==============================================================*/
/* Index: DEPTOR_TASK_FK                                        */
/*==============================================================*/
create  index DEPTOR_TASK_FK on ASSIGNEDTASK (
DEBTOR_ID
);

/*==============================================================*/
/* Index: ASSIGNEDTASK_TASK_FK                                  */
/*==============================================================*/
create  index ASSIGNEDTASK_TASK_FK on ASSIGNEDTASK (
TASK_ID,
"TIMESTAMP"
);

/*==============================================================*/
/* Table: CITY                                                  */
/*==============================================================*/
create table CITY (
   CITY_CODE            IDENTIFIER           not null,
   CITY_NAME            CITYNAME             not null,
   constraint PK_CITY primary key (CITY_CODE)
);

/*==============================================================*/
/* Index: CITY_PK                                               */
/*==============================================================*/
create unique index CITY_PK on CITY (
CITY_CODE
);

/*==============================================================*/
/* Table: CITY_WORKER                                           */
/*==============================================================*/
create table CITY_WORKER (
   WORKER_ID            ID                   not null,
   CITY_CODE            IDENTIFIER           not null,
   WORKER_NAME          NAME                 not null,
   constraint PK_CITY_WORKER primary key (WORKER_ID)
);

/*==============================================================*/
/* Index: CITY_WORKER_PK                                        */
/*==============================================================*/
create unique index CITY_WORKER_PK on CITY_WORKER (
WORKER_ID
);

/*==============================================================*/
/* Index: CITY_CITYWORKER_FK                                    */
/*==============================================================*/
create  index CITY_CITYWORKER_FK on CITY_WORKER (
CITY_CODE
);

/*==============================================================*/
/* Table: CREDITOR                                              */
/*==============================================================*/

create table CREDITOR (
    CREDITOR_ID                 ID           NOT NULL,
   CREDITOR_NAME                 TEXT        not null,
   constraint PK_CREDITOR primary key (CREDITOR_ID)
);

/*==============================================================*/
/* Index: CREDITOR_PK                                           */
/*==============================================================*/
create unique index CREDITOR_PK on CREDITOR (
CREDITOR_NAME
);

/*==============================================================*/
/* Table: DEBTOR                                                */
/*==============================================================*/
create table DEBTOR (
   DEBTOR_ID            ID                   not null,
   WORKER_ID            ID                   not null,
   constraint PK_DEBTOR primary key (DEBTOR_ID)
);

/*==============================================================*/
/* Index: DEBTOR_PK                                             */
/*==============================================================*/
create unique index DEBTOR_PK on DEBTOR (
DEBTOR_ID
);

/*==============================================================*/
/* Index: CITYWORKER_DEBTOR_FK                                  */
/*==============================================================*/
create  index CITYWORKER_DEBTOR_FK on DEBTOR (
WORKER_ID
);

/*==============================================================*/
/* Table: DEBT_IN_EURO                                          */
/*==============================================================*/
create table DEBT_IN_EURO (
   DEBT_ID              ID                   not null,
   CREDITOR_NAME                 TEXT        not null,
   DEBTOR_ID            ID                   not null,
   DEBT_DATE            DATE                 not null,
   "TIMESTAMP"          "TIMESTAMP"          not null default current_timestamp,
   AMOUNT_IN_EURO       AMOUNT               not null,
   VALID_UNTIL          "TIMESTAMP"          null,
   PAYMENT_STATUS       STATUS               null,
   constraint PK_DEBT_IN_EURO primary key (DEBT_ID, CREDITOR_NAME, DEBTOR_ID, DEBT_DATE, "TIMESTAMP")
);

/*==============================================================*/
/* Index: DEBT_IN_EURO_PK                                       */
/*==============================================================*/
create unique index DEBT_IN_EURO_PK on DEBT_IN_EURO (
DEBT_ID,
CREDITOR_NAME,
DEBTOR_ID,
DEBT_DATE,
"TIMESTAMP"
);

/*==============================================================*/
/* Index: CREDITOR_DEPT_FK                                      */
/*==============================================================*/
create  index CREDITOR_DEBT_FK on DEBT_IN_EURO (
CREDITOR_NAME
);

/*==============================================================*/
/* Index: OWED_BY_FK                                            */
/*==============================================================*/
create  index OWED_BY_FK on DEBT_IN_EURO (
DEBTOR_ID
);

/*==============================================================*/
/* Table: TASK                                                  */
/*==============================================================*/
create table TASK (
   "TIMESTAMP"          "TIMESTAMP"          not null default current_timestamp,
   TASK_ID              ID                   not null,
   WORKER_ID            ID                   null,
   DEBTOR_ID            ID                   null,
   TASK_NAME            TASK_NAME            not null,
   STATUS               STATUS               not null,
   SOCIALCOINS          SOCIALCOINS          not null,
   BEGIN_DATE           DATE                 not null,
   END_DATE             DATE                 not null,
   VALID_UNTIL          "TIMESTAMP"          null,
   constraint PK_TASK primary key (TASK_ID, "TIMESTAMP")
);

/*==============================================================*/
/* Index: TASK_PK                                               */
/*==============================================================*/
create unique index TASK_PK on TASK (
TASK_ID,
"TIMESTAMP"
);

/*==============================================================*/
/* Index: CITYWORKER_TASK_FK                                    */
/*==============================================================*/
create  index CITYWORKER_TASK_FK on TASK (
WORKER_ID
);

alter table ACTIVITY
   add constraint FK_ACTIVITY_ASSIGNEDT_ASSIGNED foreign key (DEBTOR_ID, TASK_ID)
      references ASSIGNEDTASK (DEBTOR_ID, TASK_ID)
      on delete restrict on update restrict;

alter table ACTIVITY
   add constraint FK_ACTIVITY_CITY_WORK_CITY_WOR foreign key (WORKER_ID)
      references CITY_WORKER (WORKER_ID)
      on delete restrict on update restrict;

alter table ASSIGNEDTASK
   add constraint FK_ASSIGNED_ASSIGNEDT_TASK foreign key (TASK_ID)
      references TASK (TASK_ID)
      on delete restrict on update restrict;

alter table ASSIGNEDTASK
   add constraint FK_ASSIGNED_DEBTOR_TA_DEBTOR foreign key (DEBTOR_ID)
      references DEBTOR (DEBTOR_ID)
      on delete restrict on update restrict;

alter table CITY_WORKER
   add constraint FK_CITY_WOR_CITY_CITY_CITY foreign key (CITY_CODE)
      references CITY (CITY_CODE)
      on delete restrict on update restrict;

alter table DEBTOR
   add constraint FK_DEBTOR_CITYWORKE_CITY_WOR foreign key (WORKER_ID)
      references CITY_WORKER (WORKER_ID)
      on delete restrict on update restrict;

alter table DEBT_IN_EURO
   add constraint FK_DEBT_IN__CREDITOR__CREDITOR foreign key (CREDITOR_NAME)
      references CREDITOR (CREDITOR_NAME)
      on delete restrict on update restrict;

alter table DEBT_IN_EURO
   add constraint FK_DEBT_IN__OWED_BY_DEBTOR foreign key (DEBTOR_ID)
      references DEBTOR (DEBTOR_ID)
      on delete restrict on update restrict;

alter table TASK
   add constraint FK_TASK_CITYWORKE_CITY_WOR foreign key (WORKER_ID)
      references CITY_WORKER (WORKER_ID)
      on delete restrict on update restrict;

ALTER TABLE TASK
ADD COLUMN CREATED_BY VARCHAR(30) CONSTRAINT User_Log_Info_Created_By DEFAULT(CURRENT_USER);

ALTER TABLE ACTIVITY
ADD COLUMN CREATED_BY VARCHAR(30) CONSTRAINT User_Log_Info_Created_By DEFAULT(CURRENT_USER);

ALTER TABLE DEBT_IN_EURO
ADD COLUMN CREATED_BY VARCHAR(30) CONSTRAINT User_Log_Info_Created_By DEFAULT(CURRENT_USER);

ALTER TABLE ASSIGNEDTASK
ADD COLUMN CREATED_BY VARCHAR(30) CONSTRAINT User_Log_Info_Created_By DEFAULT(CURRENT_USER);
