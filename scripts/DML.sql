INSERT INTO city
VALUES (1, 'Arnhem'), (2, 'Amsterdam'), (3, 'Rotterdam');

INSERT INTO city_worker
VALUES (1, 1, 'Steve'), (2, 2, 'John'), (3, 3, 'Abdul');

INSERT INTO creditor
VALUES (1,'Belastingdienst'), (2, 'DUO'), (3, 'CJIB');

INSERT INTO task (task_id, worker_id, task_name, STATUS, socialcoins, begin_date, end_date)
VALUES (1, 1, 'street cleaning', 'open', 300, '2019-05-20', '2019-05-25');

INSERT INTO debtor
VALUES (1, 1), (2, 2);

INSERT INTO assignedtask (debtor_id, task_id)
VALUES (1, 1);

INSERT INTO activity (debtor_id, task_id, DATE, worker_id, hours, proof, approved, description)
VALUES (1, 1, '2019-05-21', 1, 5, 'foto', false, 'I have cleaned the street');

INSERT INTO debt_in_euro (debt_id, creditor_name, debtor_id, debt_date, amount_in_euro, payment_status)
VALUES (1, 'Belastingdienst', 1, '2019-05-18', 5000, 'open');