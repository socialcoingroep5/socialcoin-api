BEGIN;
SELECT plan(7);

SELECT has_table('task');
SELECT has_table('debtor');
SELECT has_table('assignedtask');

SELECT has_function('assigntask', ARRAY['id', 'id']);

-- Here we insert dummy data to test with.
-- Because the transaction keeps the same timestamp for some reason we added our own to the dummy data for task
-- This is to prevent a primary key constraints from happening
INSERT INTO debtor VALUES (9, 2);
INSERT INTO task ("TIMESTAMP", task_id,worker_id, task_name, status, socialcoins, begin_date, end_date) VALUES ('2020-06-05 08:23:55.574667',9, 2, 'lawn mowing', 'open', 400, '2019-05-16', '2019-05-29');

SELECT throws_ok('call assignTask(9, 9)', 'This debtor has no debts');

INSERT INTO debt_in_euro (debt_id, creditor_name, debtor_id, debt_date, amount_in_euro, payment_status)
VALUES (1, 'Belastingdienst', 9, '2019-05-19', 8000, 'open');

SELECT throws_ok('call assignTask(9, 1)', 'This task is already assigned');

SELECT lives_ok('call assignTask(9, 9)');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;
