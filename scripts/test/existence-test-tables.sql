/*
To test the SocialCoin database we will be using pgTAP (https://pgtap.org/)

This script contains existence tests of the tables from the SocialCoin database.
It will check if the all the tables exists, if the tables
have primary key(s), foreign key(s), indexes and check constraints.

The existence of the columns are tested the script existence-test-columns.sql in the /test folder.
The existence of the procedures are tested the script existence-test-procedures.sql in the /test folder.

Run the test in the terminal using sudo pg_prove -d socialcoin -U root -p 5434 -h localhost existence-test-tables.sql
For more information on pg_prove visit: https://github.com/theory/pgtap

Version:    1.3
Author:     ISE Groep 5
Date:       12-06-2019
*/

BEGIN;
SELECT plan(42);

-- 01  Schema public should have the correct tables
SELECT tables_are('public',
    ARRAY['city', 'city_worker', 'task', 'debtor', 'creditor', 'assignedtask', 'debt_in_euro', 'activity', 'audit']);

-- 02  Table activity should exist
SELECT has_table('activity');
-- 03  Table activity should have a primary key
SELECT has_pk('activity');
-- 04  Index activity_pk should exist
SELECT has_index('activity', 'activity_pk');
-- 05  Index assignedtask_activity_fk should exist
SELECT has_index('activity', 'assignedtask_activity_fk');

-- 06  Table assignedtask should exist
SELECT has_table('assignedtask');
-- 07  Table assignedtask should have a primary key
SELECT has_pk('assignedtask');
-- 08  Index assignedtask_pk should exist
SELECT has_pk('assignedtask', 'assignedtask_pk');
-- 09  Index debtor_task_fk should exist
SELECT has_pk('assignedtask', 'debtor_task_fk');
-- 10  Index assignedtask_task_fk should exist
SELECT has_pk('assignedtask', 'assigned_task_fk');

-- 11  Table city should exist
SELECT has_table('city');
-- 12  Table city should have a primary key
SELECT has_pk('city');
-- 13  Index city_pk should exist
SELECT has_index('city', 'city_pk');

-- 14  Table city_worker should exist
SELECT has_table('city_worker');
-- 15  Table city_worker should have a primary key
SELECT has_pk('city_worker');
-- 16  Index city_worker_pk should exist
SELECT has_index('city_worker', 'city_worker_pk');
-- 17  Index city_cityworker_fk should exist
SELECT has_index('city_worker', 'city_cityworker_fk');

-- 18  Table creditor should exist
SELECT has_table('creditor');
-- 19  Table creditor should have a primary key
SELECT has_pk('creditor');
-- 20  Index creditor_pk should exist
SELECT has_index('creditor', 'creditor_pk');

-- 21  Table debtor should exist
SELECT has_table('debtor');
-- 22  Table debtor should have a primary key
SELECT has_pk('debtor');
-- 23  Index debtor_pk should exist
SELECT has_index('debtor', 'debtor_pk');
-- 24  Index cityworker_debtor_fk
SELECT has_index('debtor', 'cityworker_debtor_fk');

-- 25  Table debt_in_euro should exist
SELECT has_table('debt_in_euro');
-- 26  Table debt_in_euro should have a primary key
SELECT has_pk('debt_in_euro');
-- 27  Index debt_in_euro_pk should exist
SELECT has_index('debt_in_euro', 'debt_in_euro_pk');
-- 28  Index creditor_debt_fk should exist
SELECT has_index('debt_in_euro', 'creditor_debt_fk');
-- 29  Index owed_by_fk should exist
SELECT has_index('debt_in_euro', 'owed_by_fk');

-- 30  Table task should exist
SELECT has_table('task');
-- 31  Table task should have a primary key
SELECT has_pk('task');
-- 32  Table task should have a check constraint
SELECT has_check('task');
-- 33  Index task_pk should exist
SELECT has_index('task', 'task_pk');
-- 34  Index cityworker_task_fk should exist
SELECT has_index('task', 'cityworker_task_fk');

SELECT has_table('audit');

SELECT has_pk('audit');


SELECT has_fk('activity');
SELECT has_fk('assignedtask');
SELECT has_fk('city_worker');
SELECT has_fk('debtor');
SELECT has_fk('debt_in_euro');
SELECT has_fk('task');

SELECT * FROM finish();
ROLLBACK;