BEGIN;
SELECT plan(28);

--Check if all the tables that have triggers exist
SELECT has_table('task');
SELECT has_table('assignedtask');
SELECT has_table('activity');
SELECT has_table('debt_in_euro');

--Check if all the functions that are used by triggers exist
SELECT has_function('tr_task_bi_update_history');
SELECT has_function('tr_assignedtask_bi_update_history');
SELECT has_function('tr_activity_bi_update_history');
SELECT has_function('tr_debt_in_euro_bi_update_history');

--Check if the used language of the functions is right
SELECT function_lang_is('tr_task_bi_update_history', 'plpgsql');
SELECT function_lang_is('tr_assignedtask_bi_update_history', 'plpgsql');
SELECT function_lang_is('tr_activity_bi_update_history', 'plpgsql');
SELECT function_lang_is('tr_debt_in_euro_bi_update_history', 'plpgsql');

--Check if the functions return triggers or not
SELECT function_returns('tr_task_bi_update_history', 'trigger');
SELECT function_returns('tr_assignedtask_bi_update_history', 'trigger');
SELECT function_returns('tr_activity_bi_update_history', 'trigger');
SELECT function_returns('tr_debt_in_euro_bi_update_history', 'trigger');

--Check if the triggers are created on the right tables with the right functions
SELECT trigger_is('task', 'tr_task_bi_update_history', 'tr_task_bi_update_history');
SELECT trigger_is('assignedtask', 'tr_assignedtask_bi_update_history', 'tr_assignedtask_bi_update_history');
SELECT trigger_is('activity', 'tr_activity_bi_update_history', 'tr_activity_bi_update_history');
SELECT trigger_is('debt_in_euro', 'tr_debt_in_euro_bi_update_history', 'tr_debt_in_euro_bi_update_history');

--Here we insert the first records in the tables with triggers
INSERT INTO task ("TIMESTAMP", task_id, worker_id, task_name, STATUS, socialcoins, begin_date, end_date)
VALUES ('2020-06-05 08:23:55.574667', 10, 1, 'street cleaning', 'open', 300, '2019-05-20', '2019-05-25');

INSERT INTO assignedtask ("TIMESTAMP", debtor_id, task_id)
VALUES ('2020-06-05 08:23:55.574667', 1, 10);

INSERT INTO activity ("TIMESTAMP", debtor_id, task_id, DATE, worker_id, hours, proof, approved, description)
VALUES ('2020-06-05 08:23:55.574667', 1, 10, '2019-05-21', 1, 5, 'foto', false, 'I have cleaned the street');

INSERT INTO debt_in_euro ("TIMESTAMP", debt_id, creditor_name, debtor_id, debt_date, amount_in_euro, payment_status)
VALUES ('2020-06-05 08:23:55.574667', 10, 'Belastingdienst', 1, '2019-05-18', 5000, 'open');

--When we check the queries they returns nothing because the valid until column is null
SELECT is_empty('SELECT * FROM task WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT is_empty('SELECT * FROM assignedtask WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT is_empty('SELECT * FROM activity WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT is_empty('SELECT * FROM debt_in_euro WHERE debt_id = 1 AND valid_until IS NOT NULL');

--After inserting the second record the trigger updates the last record and adds a valid until
INSERT INTO task (task_id, worker_id, task_name, STATUS, socialcoins, begin_date, end_date)
VALUES (10, 1, 'street cleaning', 'open', 300, '2019-05-20', '2019-05-25');

INSERT INTO assignedtask (debtor_id, task_id, finish_date)
VALUES (1, 10, '2019-05-30');

INSERT INTO activity (debtor_id, task_id, DATE, worker_id, hours, proof, approved, description)
VALUES (1, 10, '2019-05-21', 1, 5, 'foto', false, 'I have cleaned the street');

INSERT INTO debt_in_euro (debt_id, creditor_name, debtor_id, debt_date, amount_in_euro, payment_status)
VALUES (10, 'Belastingdienst', 1, '2019-05-18', 5000, 'closed');

--Now the same queries do return data
SELECT isnt_empty('SELECT * FROM task WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT isnt_empty('SELECT * FROM assignedtask WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT isnt_empty('SELECT * FROM activity WHERE task_id = 10 AND valid_until IS NOT NULL');
SELECT isnt_empty('SELECT * FROM debt_in_euro WHERE debt_id = 10 AND valid_until IS NOT NULL');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;
