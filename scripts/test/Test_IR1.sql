BEGIN;
SELECT plan(7);

SELECT has_table('task');
SELECT has_table('debtor');
SELECT has_table('assignedtask');
SELECT has_table('activity');

SELECT has_function('sp_finish_task', ARRAY['id', 'id', 'date']);

-- Here we insert dummy data to test with.
-- Because the transaction keeps the same timestamp for some reason we added our own to the dummy data for task
-- This is to prevent a primary key constraints from happening
INSERT INTO debtor VALUES (9, 2);
INSERT INTO task ("TIMESTAMP", task_id,worker_id, task_name, status, socialcoins, begin_date, end_date) VALUES ('2020-06-05 08:23:55.574667',9, 2, 'lawn mowing', 'open', 400, '2019-05-16', '2019-05-29');
INSERT INTO assignedtask (debtor_id, task_id, "TIMESTAMP", finish_date) VALUES(9, 9, '2020-06-07 08:23:55.574667', null);
INSERT INTO activity (debtor_id, task_id, "TIMESTAMP", DATE, worker_id, hours, proof, approved, description)
VALUES (9, 9, '2020-06-06 08:23:55.574667', '2019-05-21', 1, 5, 'foto', false, 'I have cleaned the street');

SELECT throws_ok('call sp_finish_task(9, 9, ''2019-06-12'')', 'The current task is not finished, not all activities have been approved.');

INSERT INTO activity (debtor_id, task_id, "TIMESTAMP", DATE, worker_id, hours, proof, approved, description)
VALUES (9, 9, '2020-09-05 08:23:55.574667', '2019-05-21', 1, 5, 'foto', true, 'I have cleaned the street');

SELECT lives_ok('call sp_finish_task(9, 9, ''2019-07-12'')');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;
select * from assignedtask