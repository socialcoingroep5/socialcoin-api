BEGIN;
SELECT plan(4);

SELECT has_table('task');

SELECT throws_ok('call assignTask(9, 9)', 'This debtor has no debts');

SELECT throws_ok('INSERT INTO task (task_id, worker_id, task_name, STATUS, socialcoins, begin_date, end_date)
VALUES (1, 1, ''street cleaning'', ''open'', 300, ''2019-05-26'', ''2019-05-25'');', 'new row for relation "task" violates check constraint "date_check"');

SELECT lives_ok('INSERT INTO task (task_id, worker_id, task_name, STATUS, socialcoins, begin_date, end_date)
VALUES (1, 1, ''street cleaning'', ''open'', 300, ''2019-05-21'', ''2019-05-25'');');

-- Finish the tests and clean up.
SELECT * FROM finish();
ROLLBACK;