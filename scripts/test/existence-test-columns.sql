/*
To test the SocialCoin database we will be using pgTAP (https://pgtap.org/)

This script contains existence tests of the columns from the SocialCoin database.
It will check if the all the columns exists in each table, it will test of the primary keys are correct and what datatype the column accepts.
In the future we could try to test the default values of some columns, but since this is just the initial version it will not be done.

The existence of the tables are tested the script existence-test-tables.sql in the /test folder.
The existence of the procedures are tested the script existence-test-procedures.sql in the /test folder.

Run the test in the terminal using sudo pg_prove -d socialcoin -U root -p 54342 -h localhost existence-test-columns.sql
For more information on pg_prove visit: https://github.com/theory/pgtap

Version:    1.2
Author:     ISE Groep 5
Date:       12-06-2019
*/
BEGIN;
SELECT plan(104);

SELECT columns_are('activity',
    ARRAY['debtor_id', 'task_id', 'TIMESTAMP', 'date', 'worker_id', 'hours', 'proof', 'approved', 'description', 'valid_until']);

-- All columns from table activity
SELECT col_is_pk('activity', ARRAY['debtor_id', 'task_id', 'TIMESTAMP', 'date']);

SELECT has_column('activity', 'debtor_id');
SELECT col_type_is('activity', 'debtor_id', 'id');

SELECT has_column('activity', 'task_id');
SELECT col_type_is('activity', 'task_id', 'id');

SELECT has_column('activity', 'TIMESTAMP');
SELECT col_type_is('activity', 'TIMESTAMP', 'TIMESTAMP');

SELECT has_column('activity', 'date');
SELECT col_type_is('activity', 'date', 'date');

SELECT has_column('activity', 'hours');
SELECT col_type_is('activity', 'hours', 'hours');

SELECT has_column('activity', 'proof');
SELECT col_type_is('activity', 'proof', 'proof');

SELECT has_column('activity', 'approved');
SELECT col_type_is('activity', 'approved', 'approved');

SELECT has_column('activity', 'description');
SELECT col_type_is('activity', 'description', 'description');

SELECT has_column('activity', 'valid_until');
SELECT col_type_is('activity', 'valid_until', 'TIMESTAMP');

-- All columns from table assignedtask
SELECT col_is_pk('assignedtask', ARRAY['debtor_id', 'task_id', 'TIMESTAMP']);

SELECT has_column('assignedtask', 'debtor_id');
SELECT col_type_is('assignedtask', 'debtor_id', 'id');

SELECT has_column('assignedtask', 'task_id');
SELECT col_type_is('assignedtask', 'task_id', 'id');

SELECT has_column('assignedtask', 'TIMESTAMP');
SELECT col_type_is('assignedtask', 'TIMESTAMP', 'TIMESTAMP');

SELECT has_column('assignedtask', 'finish_date');
SELECT col_type_is('assignedtask', 'finish_date', 'date');

SELECT has_column('assignedtask', 'valid_until');
SELECT col_type_is('assignedtask', 'valid_until', 'TIMESTAMP');

-- All columns form table city
SELECT col_is_pk('city', 'city_code');

SELECT has_column('city', 'city_code');
SELECT col_type_is('city', 'city_code', 'identifier');

SELECT has_column('city', 'city_name');
SELECT col_type_is('city', 'city_name', 'cityname');

-- All columns from city_worker
SELECT col_is_pk('city_worker', 'worker_id');

SELECT has_column('city_worker', 'worker_id');
SELECT col_type_is('city_worker', 'worker_id', 'id');

SELECT has_column('city_worker', 'city_code');
SELECT col_type_is('city_worker', 'city_code', 'identifier');

SELECT has_column('city_worker', 'worker_name');
SELECT col_type_is('city_worker', 'worker_name', 'name');

-- All columns from creditor
SELECT col_is_pk('creditor', 'creditor_name');

SELECT has_column('creditor', 'creditor_name');
SELECT col_type_is('creditor', 'creditor_name', 'text');

-- All columns from debtor
SELECT col_is_pk('debtor', 'debtor_id');
SELECT col_is_fk('debtor', 'worker_id');

SELECT has_column('debtor', 'debtor_id');
SELECT col_type_is('debtor', 'debtor_id', 'id');

SELECT has_column('debtor', 'worker_id');
SELECT col_type_is('debtor', 'worker_id', 'id');

-- All columns from debt_in_euro
SELECT col_is_pk('debt_in_euro', ARRAY['debt_id', 'creditor_name', 'debtor_id', 'debt_date', 'TIMESTAMP']);

SELECT has_column('debt_in_euro', 'debt_id');
SELECT col_type_is('debt_in_euro', 'debt_id', 'id');

SELECT has_column('debt_in_euro', 'creditor_name');
SELECT col_type_is('debt_in_euro', 'creditor_name', 'text');

SELECT has_column('debt_in_euro', 'debtor_id');
SELECT col_type_is('debt_in_euro', 'debtor_id', 'id');

SELECT has_column('debt_in_euro', 'debt_date');
SELECT col_type_is('debt_in_euro', 'debt_date', 'date');

SELECT has_column('debt_in_euro', 'TIMESTAMP');
SELECT col_type_is('debt_in_euro', 'TIMESTAMP', 'TIMESTAMP');

SELECT has_column('debt_in_euro', 'amount_in_euro');
SELECT col_type_is('debt_in_euro', 'amount_in_euro', 'amount');

SELECT has_column('debt_in_euro', 'valid_until');
SELECT col_type_is('debt_in_euro', 'valid_until', 'TIMESTAMP');

SELECT has_column('debt_in_euro', 'payment_status');
SELECT col_type_is('debt_in_euro', 'payment_status', 'status');

-- All columns from task
SELECT col_is_pk('task', ARRAY['task_id', 'TIMESTAMP']);
SELECT col_is_fk('task', 'worker_id');

SELECT has_column('task', 'TIMESTAMP');
SELECT col_type_is('task', 'TIMESTAMP', 'TIMESTAMP');

SELECT has_column('task', 'task_id');
SELECT col_type_is('task', 'task_id', 'id');

SELECT has_column('task', 'worker_id');
SELECT col_type_is('task', 'worker_id', 'id');

SELECT has_column('task', 'task_name');
SELECT col_type_is('task', 'task_name', 'task_name');

SELECT has_column('task', 'status');
SELECT col_type_is('task', 'status', 'status');

SELECT has_column('task', 'socialcoins');
SELECT col_type_is('task', 'socialcoins', 'socialcoins');

SELECT has_column('task', 'begin_date');
SELECT col_type_is('task', 'begin_date', 'date');

SELECT has_column('task', 'end_date');
SELECT col_type_is('task', 'end_date', 'date');

SELECT has_column('task', 'valid_until');
SELECT col_type_is('task', 'valid_until', 'TIMESTAMP');

-- All column from audit
SELECT col_is_pk('audit', ARRAY['audit_id']);

SELECT has_column('audit', 'audit_id');
SELECT col_type_is('audit', 'audit_id', 'integer');

SELECT has_column('audit', 'TIMESTAMP');
SELECT col_type_is('audit', 'TIMESTAMP', 'TIMESTAMP');

SELECT has_column('audit', 'sp_name');
SELECT col_type_is('audit', 'sp_name', 'sp_name');

SELECT has_column('audit', 'sp_param');
SELECT col_type_is('audit', 'sp_param', 'sp_param');

SELECT has_column('audit', 'query_type');
SELECT col_type_is('audit', 'query_type', 'query_type');

SELECT has_column('audit', 'ip_address');
SELECT col_type_is('audit', 'ip_address', 'ip_address');

SELECT has_column('audit', 'CURRENT_USER');
SELECT col_type_is('audit', 'CURRENT_USER', 'CURRENT_USER');


SELECT * FROM finish();
ROLLBACK;