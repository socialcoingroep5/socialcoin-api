-- Start transaction and plan the tests.
BEGIN;
SELECT plan(8);

-- Run the tests.

SELECT has_table('debtor');
--###############################
--test procedure INSERT activity
--testing if a activity cannot be inserted after the finish date
--###############################
-- does function exist?

SELECT has_function('insert_activity', ARRAY ['integer', 'integer', 'date','integer','hours', 'proof',
  'approved', 'description']);

-- insert fake data

INSERT into task("TIMESTAMP",task_id, worker_id, task_name, status, socialcoins, begin_date, end_date)
VALUES ((now() - interval '1 hour' ),55, 1, 'testtask', 'open', 25, '2019-05-21', '2019-05-23');
INSERT INTO assignedtask(debtor_id, task_id, finish_date)
VALUES (1, 55, '2019-05-31');

--test success

-- test inserting a activity between the start and finish date.
SELECT lives_ok(
           'call insert_activity(1, 55, cast(''2019-05-22'' AS Date), 1, 2, ''some proof'', TRUE, ''some description'')');

--test fail expects an exception to be thrown

--test inserting a activity past the finish date of the task.
SELECT throws_ok('call insert_activity(1,55,cast(''2019-05-24'' AS Date),1,2,cast(''some proof'' as proof),TRUE, cast(''some description'' as description))
-- ','Activity date must be between the start date and end date of the corresponding task.');

--################################
--test procedure validate_task
--################################
-- does function exist?
SELECT has_function('validate_task', ARRAY['id','status']);

-- Insert fake date
INSERT into task("TIMESTAMP",task_id, worker_id, task_name, status, socialcoins, begin_date, end_date)
VALUES((now() - INTERVAL '1 hour') ,56, 1, 'testtask', 'test', 25,'2019-05-21', '2019-05-23');
INSERT INTO assignedtask(debtor_id, task_id, finish_date)
VALUES (2,56,'2019-05-31');
INSERT INTO activity(debtor_id, task_id, date, worker_id, hours, proof, approved, description)
VALUES (2,56,'2019-05-22',1,2, 'some proof', FALSE, 'some description 2');


-- test succes validating a task
 SELECT lives_ok('call validate_task(''55'',''COMPLETED'')');
 SELECT lives_ok('call validate_task(''56'',''IN PROGRESS'')');

-- test fail
 SELECT throws_ok('call validate_task(''56'',''COMPLETED'')','Not all activities for this task have been approved.');

-- Finish the tests and clean up.
SELECT *
FROM finish();
ROLLBACK;

SELECT *
from activity