/*
To test the SocialCoin database we will be using pgTAP (https://pgtap.org/)

This script contains existence tests of the procedures from the SocialCoin database.

The existence of the columns are tested the script existence-test-columns.sql in the /test folder.
The existence of the tables are tested the script existence-test-tables.sql in the /test folder.

Run the test in the terminal using sudo pg_prove -d socialcoin -U root -p 5434 -h localhost existence-test-tables.sql
For more information on pg_prove visit: https://github.com/theory/pgtap

Version:    1.1
Author:     ISE Groep 5
Date:       12-06-2019
*/

BEGIN;
SELECT plan(28);

SELECT has_function('insert_activity', ARRAY['id', 'id', 'date', 'id', 'hours', 'proof', 'approved', 'description', 'ip_address']);
SELECT has_function('insert_debtor', ARRAY['integer', 'ip_address']);
SELECT has_function('insert_creditor', ARRAY['name', 'ip_address']);
SELECT has_function('insert_city_worker', ARRAY['name', 'name', 'ip_address']);
SELECT has_function('city', ARRAY['name']);
SELECT has_function('insert_open_debt', ARRAY['name', 'id', 'date', 'amount', 'ip_address']);
SELECT has_function('insert_task', ARRAY['id', 'id', 'name', 'status', 'socialcoins', 'date', 'date', 'ip_address']);
SELECT has_function('validate_task', ARRAY['id', 'status', 'ip_address']);
SELECT has_function('pay_off_debt', ARRAY['id', 'ip_address']);
SELECT has_function('assigntask', ARRAY['id', 'id', 'ip_address']);
SELECT has_function('retrieve_debtor', ARRAY['id']);
SELECT has_function('retrieve_debtors');
SELECT has_function('retrieve_debt_in_euro', ARRAY['id']);
SELECT has_function('retrieve_debt_in_euro', ARRAY['id', 'ip_address']);
SELECT has_function('retrieve_all_debs_for_creditor');
SELECT has_function('retrieve_creditors', ARRAY['ip_address']);
SELECT has_function('retrieve_cityworker', ARRAY['id']);
SELECT has_function('retrieve_cityworkerforcity', ARRAY['id']);
SELECT has_function('retrieve_cities');
SELECT has_function('retrieve_tasks', ARRAY['ip_address']);
SELECT has_function('retrieve_task', ARRAY['id']);
SELECT has_function('retrieve_task', ARRAY['status', 'ip_address']);
SELECT has_function('retrieve_assignedtasks', ARRAY['ip_address']);
SELECT has_function('retrieve_assignedtaskstodebtor', ARRAY['id', 'ip_address']);
SELECT has_function('retrieve_taskactivities', ARRAY['id', 'ip_address']);
SELECT has_function('retrieve_pendingactivities', ARRAY['id']);
SELECT has_function('retrieve_allactivities');
SELECT has_function('sp_audit_insert', ARRAY['sp_name', 'sp_param', 'query_type', 'ip_address', '"CURRENT_USER"']);

SELECT * FROM finish();
ROLLBACK;
