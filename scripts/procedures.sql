/*
id: the id of the debtor that has done the activity.
id: the id of the task for which the activity was done.
date: the date on which the activity was done
id: the id of the worker overseeing this task
hours: the hours that the activity took.
proof: the proof of activity
approved: boolean whether the activity was approved or not.
description: a description of the activity
 */
CREATE OR REPLACE PROCEDURE Insert_Activity(id, id, date, id, hours, proof, approved, description, ip_address)
  LANGUAGE plpgsql
AS
$$
BEGIN
  IF $3 > (SELECT end_date FROM task WHERE task_id = $2) OR
     $3 < (SELECT begin_date from task WHERE task_id = $2) THEN
    RAISE EXCEPTION 'Activity date must be between the start date and end date of the corresponding task.';
  END IF;

   IF ($5 + (SELECT sum(hours) FROM activity WHERE date = $3 GROUP BY date)) > 8 THEN
     RAISE EXCEPTION 'A debtor cannot work more than 8 hours per day.';
   END IF;

  INSERT INTO activity( debtor_id, task_id, date, worker_id, hours, proof, approved, description)
    VALUES ($1, $2, $3, $4, $5, $6, $7, $8);
    CALL SP_Audit_Insert(CAST('Insert_Activity' AS sp_name), CAST('id, id, date, id, hours' AS sp_param), CAST('INSERT' as query_type), $9, CAST(current_user AS "CURRENT_USER"));
END;
$$;

/*
  Id = the id of the debtor.
  id = id of the worker that oversees the debtor.
  ip_address: the ip address of the computer that executed the stored procedure
 */
CREATE OR REPLACE FUNCTION Insert_Debtor( INTEGER, ip_address)
returns setof debtor
LANGUAGE plpgsql
AS $$
    DECLARE new_debtor_id ID := CAST((SELECT MAX(debtor_id) + 1 FROM debtor) as id);
  BEGIN
    INSERT INTO debtor VALUES (new_debtor_id, $1);
    CALL SP_Audit_Insert(CAST('Insert_Debtor' AS sp_name), CAST('id, id' AS sp_param), CAST('INSERT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM debtor d WHERE d.debtor_id = new_debtor_id;
  end;
  $$;
/*
  name  = the name of the creditor.

 */
CREATE OR REPLACE PROCEDURE Insert_Creditor(name, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
    INSERT INTO creditor(creditor_id, creditor_name) VALUES ((SELECT MAX(creditor_id) + 1 FROM creditor), $1);
    CALL SP_Audit_Insert(CAST('Insert_Creditor' AS sp_name), CAST('name' AS sp_param), CAST('INSERT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));

  end;
  $$;

/*
Name = the City name where the worker is active.
id = the id of the city worker.
Name = the name of the city worker.
 */
CREATE OR REPLACE PROCEDURE Insert_City_Worker(name, name, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
    INSERT INTO city_worker(city_code, worker_id, worker_name) VALUES (CAST((SELECT city_code FROM city WHERE city_name = $1) as id), CAST((SELECT MAX(worker_id) + 1 FROM city_worker) as id), $2);
    CALL SP_Audit_Insert(CAST('Insert_CityWorker' AS sp_name), CAST('id, id' AS sp_param), CAST('INSERT' as query_type), $3, CAST(current_user AS "CURRENT_USER"));

  end;
  $$;
/*
name = the name of the city.
 */
CREATE OR REPLACE PROCEDURE City(name)
LANGUAGE plpgsql
AS $$
  BEGIN
    INSERT INTO city VALUES ($1);
  end;
  $$;
/*
Name = the name of the creditor to which the debt is owed.
ID = the id of the debtor that owes the debt.
date = the date on which the debt started.
Amount = the ammount of the debt in euros.
 */
CREATE OR REPLACE PROCEDURE Insert_Open_Debt(name, id, date, amount, ip_address)
LANGUAGE plpgsql
AS $$
        BEGIN
            INSERT INTO debt_in_euro(creditor_name, debtor_id, debt_date, amount_in_euro, debt_id) VALUES ($1, $2, $3, $4, CAST(CAST((SELECT (MAX(d.debt_id) + 1) as debtId FROM debt_in_euro d) AS INT4) AS id));
            CALL SP_Audit_Insert(CAST('Insert_Open_Debt' AS sp_name), CAST('name, id, date, amount' AS sp_param), CAST('INSERT' as query_type), $5, CAST(current_user AS "CURRENT_USER"));
        end;
  $$;

/*
 Id : the id of the city worker
 name : the name of the city.
 id : the id of the worker.
 name : the name of the task.
 status : the status of the task ( suggested , approved , in progress, completed).
 socialcoins : the ammount of socialcoins the task is worth.
 date : the begin date of the task.
 date : the end date of the task.
 ip_address: the ip address of the computer that executed the stored procedure
 */
CREATE OR REPLACE  PROCEDURE  Insert_Task(id, id, name, status, socialcoins, date, date, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
    INSERT INTO task(task_id, worker_id, debtor_id, task_name, status, socialcoins, begin_date, end_date) VALUES (
    (SELECT MAX(t.task_id) + 1 as task_id from task t), $1,$2, $3, $4, $5, $6, $7);
    CALL SP_Audit_Insert(CAST('Insert_Task' AS sp_name), CAST('id, id, name, status, socialcoins, date, date' AS sp_param), CAST('INSERT' as query_type), $8, CAST(current_user AS "CURRENT_USER"));
  end;

  $$;

CREATE OR REPLACE  PROCEDURE  validate_task(id, status, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
      -- insert new
      IF $2 = 'COMPLETED' AND EXISTS(SELECT 1 FROM Activity WHERE task_id = $1 AND approved != TRUE )  THEN
        RAISE EXCEPTION 'Not all activities for this task have been approved.';
END IF;
      UPDATE assignedtask SET valid_until = CURRENT_TIMESTAMP WHERE task_id = $1;
      INSERT INTO task(debtor_id, task_id, worker_id, task_name, status, socialcoins, begin_date, end_date)
      SELECT t.debtor_id, t.task_id,
             t.worker_id, t.task_name, $2 as status, t.socialcoins, t.begin_date, t.end_date
      FROM task t WHERE t.task_id = $1 AND valid_until = null;
      CALL SP_Audit_Insert(CAST('Insert_Task' AS sp_name), CAST('id, status' AS sp_param), CAST('INSERT' as query_type), $3, CAST(current_user AS "CURRENT_USER"));
  END;
  $$;


CREATE OR REPLACE  PROCEDURE  validate_activity(id, date, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
      INSERT INTO activity( debtor_id, task_id, date, worker_id, hours, proof, approved, description)
      SELECT debtor_id, task_id, date, worker_id, hours, proof, true as approved, description FROM activity WHERE task_id = $1 AND date = $2 AND valid_until IS NULL;
      CALL SP_Audit_Insert(CAST('Insert_Activity' AS sp_name), CAST('id, id' AS sp_param), CAST('INSERT' as query_type), $3, CAST(current_user AS "CURRENT_USER"));
  END;
  $$;

CREATE OR REPLACE  PROCEDURE  pay_off_debt(id, ip_address)
LANGUAGE plpgsql
AS $$
  BEGIN
      UPDATE debt_in_euro SET valid_until = current_timestamp WHERE debt_id = $1;
  INSERT INTO debt_in_euro(creditor_name, debtor_id, debt_date, amount_in_euro, debt_id, payment_status) SELECT creditor_name, debtor_id, debt_date, amount_in_euro, $1 as debt_id, 'COMPLETED' as payment_status FROM debt_in_euro d WHERE d.debt_id = $1 AND valid_until = null;
      CALL SP_Audit_Insert(CAST('pay_off_debt' AS sp_name), CAST('id' AS sp_param), CAST('INSERT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
  END;
  $$;
/*
 Stored procedure to assign a task to a debtor, the finish date will always be null when assigning a task
 id : The id of the debtor
 id : The id of the task that will be assigned
 ip_address: the ip address of the computer that executed the stored procedure
 */
create or replace procedure assignTask(id, id, ip_address)
LANGUAGE plpgsql
AS $$
BEGIN
    IF NOT exists(SELECT 1 FROM debt_in_euro WHERE debtor_id = $1) THEN
        RAISE EXCEPTION 'This debtor has no debts';
    ELSEIF exists(SELECT 1 FROM task WHERE task_id = $2 AND status = 'Assigned') THEN
        RAISE EXCEPTION 'This task is already assigned';
    ELSE
        INSERT INTO assignedtask VALUES ($1, $2, CURRENT_TIMESTAMP);
        UPDATE task SET status = 'Assigned' WHERE task_id = $2;
        CALL SP_Audit_Insert(CAST('assignTask' AS sp_name), CAST('id, id' AS sp_param), CAST('INSERT' as query_type), $3, CAST(current_user AS "CURRENT_USER"));
    END IF;
END
$$;

/*
 Retrieve the data of a specific debtor
 id: The id of the debtor that will be retrieved
 */
CREATE OR REPLACE FUNCTION Retrieve_Debtor(id)
returns setof debtor
LANGUAGE plpgsql AS $$
  BEGIN
    RETURN QUERY SELECT * FROM debtor d WHERE d.debtor_id = $1;
  END;
  $$;


/*
 Retrieve all the debtors
 */
CREATE OR REPLACE FUNCTION Retrieve_Debtors()
RETURNS SETOF debtor
LANGUAGE plpgsql
AS $$
  BEGIN
    RETURN QUERY SELECT * FROM debtor;
  END;
  $$;


/*
 Retrieve the debt in euros by debt_id
 id: The id of the debt (debt_id)
 */
 CREATE OR REPLACE FUNCTION Retrieve_Debt_In_Euro(id)
 RETURNS SETOF debt_in_euro
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM debt_in_euro WHERE debt_id = $1 AND valid_until IS NULL;
  END;
  $$;


/*
 Retrieve the debt in euro of a debtor
 id: The id of the debtor
 ip_address: the ip address of the computer that executed the stored procedure
 */
 CREATE OR REPLACE FUNCTION Retrieve_Debt_In_Euro(id, ip_address)
 RETURNS SETOF debt_in_euro
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_Debt_In_Euro' AS sp_name), CAST('id' AS sp_name), CAST('SELECT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM debt_in_euro WHERE debtor_id = $1 AND valid_until IS NULL;
  END;
  $$;

/*
 Retrieve all the debts in euros for a creditor
 name: The name of the creditor
 */
 CREATE OR REPLACE FUNCTION Retrieve_All_Debs_For_Creditor(name)
 RETURNS SETOF debt_in_euro
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM debt_in_euro WHERE creditor_name = $1;
  END;
  $$;


/*
 Retrieve a list of all the creditors
 */
 CREATE OR REPLACE FUNCTION Retrieve_Creditors(ip_address)
 RETURNS SETOF creditor
 LANGUAGE plpgsql
 AS $$
  BEGIN
      CALL SP_Audit_Insert(CAST('Retrieve_Creditors' AS sp_name), CAST('' AS sp_name), CAST('SELECT' as query_type), $1, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM creditor;
  END;
  $$;

/*
 Retrieve the data of a specific city worker
 id: The id of the city worker
 */
 CREATE OR REPLACE FUNCTION Retrieve_CityWorker(id)
 RETURNS SETOF city_worker
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM city_worker WHERE worker_id = $1;
  END;
  $$;

/*
 Retrieve a list of all the workers for a specific city
 id: the city code
 */
 CREATE OR REPLACE FUNCTION Retrieve_CityWorkerForCity(id)
 RETURNS SETOF city_worker
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM city_worker WHERE city_code = $1;
  END;
  $$;

/*
 Retrieve all cities
 */
 CREATE OR REPLACE FUNCTION Retrieve_Cities()
 RETURNS SETOF city
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM city;
  END;
  $$;

/*
 Retrieve a list of all the tasks
 */
 CREATE OR REPLACE FUNCTION Retrieve_Tasks(ip_address)
 RETURNS SETOF task
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_Tasks' AS sp_name), NULL, CAST('SELECT' as query_type), $1, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM task WHERE valid_until IS NULL;
  END;
  $$;

/*
 Retrieve a task by id
 id: The id of the task to be retrieved
 */
 CREATE OR REPLACE FUNCTION Retrieve_Task(id)
 RETURNS SETOF task
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM task WHERE task_id = $1 AND valid_until IS NULL;
  END;
  $$;

/*
 Retrieve all tasks based on a specific status
 status: The status of a task
 ip_address: the ip address of the computer that executed the stored procedure
 */
 CREATE OR REPLACE FUNCTION Retrieve_Task(status, ip_address)
 RETURNS SETOF task
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_Task' AS sp_name), CAST('status' AS sp_param), CAST('SELECT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM task WHERE status = $1 AND valid_until IS NULL;
  END;
  $$;

/*
 retrieves a list of all assigned tasks
 ip_address: the ip address of the computer that executed the stored procedure
 */
 CREATE OR REPLACE FUNCTION Retrieve_AssignedTasks(ip_address)
 RETURNS SETOF assignedtask
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_AssignedTasks' AS sp_name), NULL, CAST('SELECT' as query_type), $1, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM assignedtask WHERE valid_until IS NULL;
  END;
  $$;

/*
 Retrieves all assigned tasks to a specific debtor
 id: The id of a debtor
 ip_address: the ip address of the computer that executed the stored procedure
 */
 CREATE OR REPLACE FUNCTION Retrieve_AssignedTasksToDebtor(id, ip_address)
 RETURNS SETOF assignedtask
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_AssignedTasksToDebtor' AS sp_name), CAST('id' AS sp_param), CAST('SELECT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM assignedtask WHERE debtor_id = $1 AND valid_until IS NULL;
  END;
  $$;
/*
 Retrieves the activities for a specific task
 id: The id of the task
 */
 CREATE OR REPLACE FUNCTION Retrieve_TaskActivities(id, ip_address)
 RETURNS SETOF activity
 LANGUAGE plpgsql
 AS $$
  BEGIN
    CALL SP_Audit_Insert(CAST('Retrieve_TaskActivities' AS sp_name), CAST('id' AS sp_param), CAST('SELECT' as query_type), $2, CAST(current_user AS "CURRENT_USER"));
    RETURN QUERY SELECT * FROM activity WHERE task_id = $1 AND valid_until IS NULL;
  END;
  $$;

/*
 Retrieves all activities that are not approved or declined for a specific debtor
 id: The id of the debtor
 */
 CREATE OR REPLACE FUNCTION Retrieve_PendingActivities(id)
 RETURNS SETOF activity
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM activity WHERE debtor_id = $1 AND approved IS NULL AND valid_until IS NULL;
  END;
  $$;

/*
 Retrieves all activities that are not approved or declined
 */
 CREATE OR REPLACE FUNCTION Retrieve_AllActivities()
 RETURNS SETOF activity
 LANGUAGE plpgsql
 AS $$
  BEGIN
    RETURN QUERY SELECT * FROM activity WHERE approved IS NULL AND valid_until IS NULL;
  END;
  $$;

/*
 Inserts the data in the audit table
 sp_name: the name of the stored procedure
 query_type: type of query, this must be either SELECT or INSERT
 ip_address: the ip address of the computer that executed the stored procedure
 current_user: the user that executed the stored procedure
 */
CREATE OR REPLACE PROCEDURE SP_Audit_Insert(sp_name, sp_param, query_type, ip_address, "CURRENT_USER")
LANGUAGE plpgsql
AS $$
  BEGIN
    INSERT INTO audit (sp_name, sp_param, query_type, ip_address, "CURRENT_USER")
    VALUES ($1, $2, $3, $4, $5);
  END;
$$;

